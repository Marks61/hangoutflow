﻿<%@ Page Title="" Language="C#" MasterPageFile="~/forgot.Master" AutoEventWireup="true" CodeBehind="forgot_passwod.aspx.cs" Inherits="hangout.forgot_passwod" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    
    <h2 class="control-label col-md-5" >* 請輸入帳號 *</h2>

   <div class="form-horizontal">
  <div class="form-group">
            
            <div class="col-md-7">
                <asp:TextBox ID="txtAcc" type="text" CssClass="form-control" placeholder="請輸入帳號" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAcc" Display="Dynamic"
                                             ErrorMessage="(必填)" ForeColor="Red" Font-Size="10pt"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" runat="server" ControlToValidate="txtAcc" ValidationExpression="\S{6,20}"
                            ErrorMessage="(格式不符)" ForeColor="Red" Font-Size="10pt"></asp:RegularExpressionValidator>
            </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="post" type="submit" runat="server" Text="送出" CssClass="btn btn-dark" OnClick="post_Click"/>
                </div>
            </div>
   </div>
       </div>
    <div  style="text-align:center;font-size:16pt;margin-top:630px;"><span id="motion">步驟 1/4 </span><asp:LinkButton ID="cancel" runat="server" CausesValidation="false"><a id="back" href="login.aspx" >取消</a></asp:LinkButton></div>
</asp:Content>
