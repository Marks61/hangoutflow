﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hangout
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
      
            if (Session["Name"] == null) {
                Response.Redirect("login.aspx");
            }
             name.Text =Session["Name"].ToString();

           logout.Attributes.Add("onclick", "return confirm( '確定要登出嗎?');");
        }

        protected void logout_Click(object sender, EventArgs e)
        {
               
            Session.RemoveAll();
            Response.Redirect("login.aspx");
        }
    }
}