﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="hangout.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta name="viewport" content="width=device-width" />
    <title>HangOut · 管理者登入</title>
    <link rel="shutcut icon" href="/images/logo/logo.jpg" />
    <!-- Bootstrap core CSS -->
<link href="Content/bootstrap4.3.1.min.css" rel="stylesheet" />
<%--<script src="showtime.js"></script>--%>

    <style>
      .bd-placeholder-img {
          
        font-size: 1.125rem;
        text-anchor: middle;

      }
        body {
            background-image:url(/images/logo/bg.jpg);
            background-size:cover ;
        background-color:transparent;
        }
      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="Content/hexschool-4.2-floating-labels.css" rel="stylesheet" />
  </head>
<body>
   <form class="form-signin" style="margin-left:40%" runat="server">
  <div class="text-center mb-4" >
    <img class="mb-4" src="/images/logo/logo.jpg" alt="" width="144" height="144" />
    <p class="h2 mb-3 font-weight-normal">HangOut後台登入</p>
    <p>請輸入管理者帳號密碼進行登入。</p>
    
  </div>

  <div class="form-label-group">
      <asp:TextBox ID="inputAccount" type="text" CssClass="form-control" placeholder="Account" required="required" autofocus="autofocus" runat="server"></asp:TextBox>
       <label for="inputAccount">Account</label>
  </div>

  <div class="form-label-group">
      <asp:TextBox ID="inputPassword" type="password" CssClass="form-control" placeholder="Password" required="required" autofocus="autofocus" runat="server"></asp:TextBox>
    <label for="inputPassword">Password</label>
  </div>
       <asp:Button ID="btnlogin" runat="server" Text="系統登入" class="btn btn-lg btn-primary btn-block" OnClick="btnlogin_Click"/>
       <asp:Label ID="labMsg" runat="server" Text="" CssClass="mt-5 mb-3 text-msg text-center" Font-Bold="true" ForeColor="Red"></asp:Label><br />
       <asp:LinkButton ID="LinkButton1" align="center" runat="server" OnClick="LinkButton1_Click">忘記密碼</asp:LinkButton>
 <p class="mt-5 mb-3 text-muted text-center"> &copy; <%: DateTime.Now.Year %>·HangOut版權所有</p>
      
</form> 
    <asp:Label ID="Label1" runat="server" Text="" style="margin-right:10px;margin-top:-900px;font-size:14px;"></asp:Label>
    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
</body>
</html>
