﻿<%@ Page Title="" Language="C#" MasterPageFile="~/forgot.Master" AutoEventWireup="true" CodeBehind="notfound_account.aspx.cs" Inherits="hangout.notfound_account" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h1 style="color:red">出現錯誤!!<br /><small>找不到您的帳號。請確認資料都輸入正確。</small></h1>
     <div  style="text-align:center;font-size:16pt;margin-top:650px;"><span id="motion">步驟 4/4 </span><asp:LinkButton ID="cancel" runat="server" CausesValidation="false"><a id="back" href="login.aspx" >回到登入頁</a></asp:LinkButton></div>
</asp:Content>
