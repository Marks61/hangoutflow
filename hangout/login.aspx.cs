﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using hangout.App_Code; //執行轉湊轉換方法
using System.Net.Http;

namespace hangout
{

    public partial class login : System.Web.UI.Page
    {SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);
                 
        
        protected void Page_Load(object sender, EventArgs e)
        { 
            //顯示登入者ip
            string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();

            Label1.Text = "登入者IP:"+tClientIP;
        }

        protected void btnlogin_Click(object sender, EventArgs e)

        {
            //讀取帳號/密碼(轉換為雜湊系統)
           string PASS;    
            string account = inputAccount.Text;
            pass password = new pass();
            PASS =password.Change(inputPassword.Text);
           
            //登入
            SqlCommand cdm = new SqlCommand("select * from administrator where admAcc=@Act", cdnn);
            cdm.Parameters.AddWithValue("@Act",account);
            SqlDataReader rd;
        
            cdnn.Open();
            rd = cdm.ExecuteReader();
            if (rd.HasRows)
            {

                while (rd.Read())
                {
                    
                    string pasw = rd["admPsw"].ToString();
                    if (pasw == PASS)
                    {
                        Context.Session["Name"] = rd["admName"];
                        Context.Session["Account"] = rd["admAcc"];
                        Context.Session["Id"] = rd["admNo"];
                        Context.Session["password"] = rd["admPsw"];
                        Context.Session["passer"] = rd["admHighest"].ToString();
                        Response.Redirect("index.aspx");
                    }
                    else
                    {
                        labMsg.Text = "登入驗證失敗";
                        //Response.Write(pasw.GetHashCode().ToString()); 
                        //Response.Write(PASS);
                        //Response.Write(rd["admPsw"]);
                    }

                }

            }
            else {
                labMsg.Text = "登入驗證失敗";
            }
            cdnn.Close();
          
        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {//忘記密碼
            Response.Redirect("forgot_passwod.aspx");
        }
    }
   
}