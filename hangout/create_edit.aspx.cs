﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{

    public partial class create_edit : System.Web.UI.Page
    {

        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);
        SqlConnection cenn = new SqlConnection(ConfigurationManager.ConnectionStrings["Hangoutnews"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            txtDate.Text = DateTime.Now.ToString();
            if (IsPostBack) { }
            else
            {
                SqlCommand drop = new SqlCommand("select * from member", cdnn);
                SqlDataReader drpr;
                cdnn.Open();
                drpr = drop.ExecuteReader();

                ListItem item;
                DropDownList1.Items.Clear();

                while (drpr.Read())
                {
                    item = new ListItem(drpr["memAcc"].ToString(), drpr["memNo"].ToString());
                    DropDownList1.Items.Add(item);
                }
                cdnn.Close();
          
            }
            if (DropDownList1.SelectedValue !="null")
            {
                SqlCommand memberr = new SqlCommand("select * from member where memNo=@code", cdnn);

                memberr.Parameters.AddWithValue("@code", DropDownList1.SelectedValue);
                SqlDataReader rd;

                cdnn.Open();
                rd = memberr.ExecuteReader();
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        Label2.Text = rd["memName"].ToString();
                    }
                }
                cdnn.Close();



            }
            else
            {
              
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (DropDownList1.SelectedValue != "null")
            {
                SqlCommand memberr = new SqlCommand("select * from member where memNo=@code", cdnn);

                memberr.Parameters.AddWithValue("@code", DropDownList1.SelectedValue);
                SqlDataReader rd;

                cdnn.Open();
                rd = memberr.ExecuteReader();
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        Label2.Text = rd["memName"].ToString();
                    }
                }
                cdnn.Close();



            }
            else
            {

            }


        }

        protected void submit_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("insert into mesAdm(memRec,mesAdmSub,mesAdmCon,mesAdmDate,mesAdmImg,admNo) values(@rec,@title,@content,@date,@pic,@no)", cdnn);
                
                  cdnn.Open();
                
                cmd.Parameters.AddWithValue("@rec", DropDownList1.SelectedValue);
                cmd.Parameters.AddWithValue("@title", txtTitle.Text);
                cmd.Parameters.AddWithValue("@content", txtContent.Text);
                cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(txtDate.Text));
              
                if (FileUpload1.PostedFile.ContentType == "image/jpeg" || FileUpload1.PostedFile.ContentType == "image/gif" || FileUpload1.PostedFile.ContentType == "image/png" || FileUpload1.PostedFile.ContentType == "image/bmp")
                {
                    cmd.Parameters.AddWithValue("@pic", FileUpload1.FileBytes);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@pic",null);
                }

                cmd.Parameters.AddWithValue("@no", Session["Id"].ToString());
   cmd.ExecuteNonQuery();
                cmd.Clone();
                Label1.Text = "訊息傳送成功。";
             
            }
            catch (Exception ex)
            {

                Label1.Text = ex.Message + "留言傳送失敗。";
            }
        }

        protected void btncancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }
    }
}
    
