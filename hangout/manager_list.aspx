﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="manager_list.aspx.cs" Inherits="hangout.manager_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>管理者帳號管理</h2>
    
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HangOutlogin %>" SelectCommand="SELECT * FROM [administrator]"
        DeleteCommand="delete administrator where admNo=@admNo"></asp:SqlDataSource>
    <asp:GridView ID="GridView1" autoGenerateColumns="False" GridLines="None" runat="server" OnRowCommand="GridView1_RowCommand" DataKeyNames="admNo" DataSourceID="SqlDataSource1" PageSize="20" CssClass="table">
                <Columns>
            <asp:BoundField DataField="admNo" HeaderText="管理者編號" ReadOnly="True" SortExpression="admNo" />
            <asp:BoundField DataField="admName" HeaderText="管理者名稱" SortExpression="admName" />
            <asp:BoundField DataField="admAcc" HeaderText="管理者帳號" SortExpression="admAcc" />
            <asp:CheckBoxField DataField="admHighest" HeaderText="最高權限" SortExpression="admHighest" />
            <asp:BoundField DataField="admMail" HeaderText="信箱" SortExpression="admMail" />
            <asp:TemplateField ControlStyle-Font-Names="操作">
                <ItemTemplate>
       <asp:Button runat="server" ID="btnEdit" Text="編輯" CssClass="btn btn-warning" CommandName="Edit" CommandArgument='<%# Eval("admNo")  %>' />
                <asp:Button runat="server" ID="btnDelete" Text="刪除" CssClass="btn btn-danger" CommandName="Deleting" OnClientClick="return confirm('確認刪除帳號?')" UseSubmitBehavior="true" CommandArgument='<%#Eval("admNo") %>' />
                </ItemTemplate>
         
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
