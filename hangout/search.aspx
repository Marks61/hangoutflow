﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="search.aspx.cs" Inherits="hangout.search" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="color:red">帳號查詢</h2>
    <div class="row">
        <div class="col-lg-4">

<div class="card">
  <div class="card-header">
    <h4>搜尋管理者</h4>
  </div>
 <ul class="list-group list-group-flush">
        <asp:TextBox ID="TextBox1" class="" CssClass="list-group-item" placeholder="請輸入帳號" runat="server"></asp:TextBox>
       <li class="list-group-item"> <asp:Button ID="Button1" runat="server" Text="搜尋" CssClass="btn btn-dark" OnClick="Button1_Click" /></li>
  </ul>
</div>
        <div class="card">
  <div class="card-header">
    <h4>搜尋會員</h4>
  </div>
  <ul class="list-group list-group-flush">
        <asp:TextBox ID="TextBox2" class="" CssClass="list-group-item" placeholder="請輸入帳號" runat="server"></asp:TextBox>
       <li class="list-group-item"> <asp:Button ID="Button2" runat="server" Text="搜尋" CssClass="btn btn-dark" OnClick="Button2_Click" /></li>
  </ul>
</div>
        </div>
       
    <div >
        <iframe src="" class="col-lg-8" id="ser" runat="server" style="height:620px;border:none"></iframe>
    </div>
        </div>
</asp:Content>
