﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{
    public partial class Create_news : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["hangOutnews"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            txtAdm.Text = Session["Name"].ToString();
            txtDate.Text = DateTime.Now.ToString();
        }

        protected void submit_Click(object sender, EventArgs e)
        {
                try
                {
                    SqlCommand cmd = new SqlCommand("insert into news values(@title,@content,@date,@bool,@no)", cdnn);
                    cmd.Parameters.AddWithValue("@no", Session["Id"].ToString());
                    cmd.Parameters.AddWithValue("@title", txtTitle.Text);
                    cmd.Parameters.AddWithValue("@content", txtContent.Text);
                    cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(txtDate.Text));
                    if (CheckBox1.Checked)
                    {
                        cmd.Parameters.AddWithValue("@bool", true);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@bool", false);
                    }
                    cdnn.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Clone();
                    Response.Redirect("news.aspx");
                }
                catch (Exception ex) {
                
                    Label1.Text =ex.Message+"消息新增失敗。";
                }
            

        }

        protected void btncancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("news.aspx");
        }
    }
}