﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace hangout
{
    public partial class contact : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["Hangoutnews"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //新增一個gridview 列出所有 actNo為null的資料

            SqlCommand sql2 = new SqlCommand("SELECT * FROM contact INNER JOIN member ON member.memNo=contact.memNo  where admNo is null", cdnn);
            SqlDataAdapter d = new SqlDataAdapter(sql2);
            DataSet dt = new DataSet();
            d.Fill(dt, "Contact");
            this.gridview2.DataSource = dt;
            this.gridview2.DataBind();

            //
            SqlCommand sql = new SqlCommand("SELECT * FROM  (contact INNER JOIN member ON contact.memNo = member.memNo)INNER JOIN administrator ON contact.admNo = administrator.admNo where administrator.admNo=@id", cdnn);
            sql.Parameters.AddWithValue("@id", Session["Id"].ToString());
            SqlDataAdapter dR=new SqlDataAdapter(sql);
            DataSet de = new DataSet();
            dR.Fill(de,"Contact");
                 this.gridview1.DataSource = de;
            this.gridview1.DataBind();
        
   
        }

        protected void gridview1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "info")
            {
                Response.Redirect("RE_MAIL.aspx?contactNo=" + e.CommandArgument.ToString());
            }
        }
    }
}
