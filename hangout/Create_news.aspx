﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Create_news.aspx.cs" Inherits="hangout.Create_news" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>新增消息</h2>
    <asp:Label ID="Label1" runat="server" Text="" Font-Bold="true" ForeColor="Red"></asp:Label>
    <hr />
    <div class="form-horizontal">
         <div class="form-group">
            <asp:label runat="server" Cssclass="control-label col-md-2" for="txtAdm" text="發布者:" font-Bold="true"></asp:label>
            <asp:textbox ID="txtAdm" Cssclass="form-control col-md-10" readOnly="true" runat="server"></asp:textbox>
        </div>
         <div class="form-group">
            <asp:label runat="server" Cssclass="control-label col-md-2" for="txtTitle" text="標題:" font-Bold="true"></asp:label>
            <asp:textbox ID="txtTitle" Cssclass="form-control col-md-10" runat="server"></asp:textbox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTitle" ErrorMessage="(必填)" ForeColor="Red" Font-Size="12pt"></asp:RequiredFieldValidator>

        </div>
         <div class="form-group">
            <asp:label runat="server" Cssclass="control-label col-md-2" for="txtContent" text="內容:" font-Bold="true"></asp:label>
            <asp:textbox ID="txtContent" Cssclass="form-control col-md-10" TextMode="MultiLine" Height="200px" runat="server"></asp:textbox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtContent" ErrorMessage="(必填)" ForeColor="Red" Font-Size="12pt"></asp:RequiredFieldValidator>
        </div>
        <div class="form-group">
            <asp:label runat="server" Cssclass="control-label col-md-2" for="txtDate" text="發布日期:" font-Bold="true"></asp:label>
            <asp:textbox ID="txtDate" Cssclass="form-control col-md-10" readOnly="true" runat="server"></asp:textbox>
        </div>
          <div class="form-group">
            <label class="control-label col-md-2">是否顯示:</label>
            <div class="col-md-10">
                <asp:CheckBox ID="CheckBox1" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="submit" CssClass="btn btn-primary" runat="server" Text="送出" OnClick="submit_Click"/>
                 <asp:Button ID="btncancle" CssClass="btn btn-danger" runat="server" Text="取消"  CausesValidation="false" OnClick="btncancle_Click" />
            </div>
        </div>
    </div>
</asp:Content>
