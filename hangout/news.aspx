﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="news.aspx.cs" Inherits="hangout.news" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>消息管理</h2>
    <asp:SqlDataSource ID="hangOutnews" runat="server" ConnectionString="<%$ ConnectionStrings:hangOutnews %>" OnSelecting="hangOutnews_Selecting"
        SelectCommand="select * from news inner join administrator on news.admNo=administrator.admNo order by newsDate desc"
        DeleteCommand="select * from news where newsNo=@newsNo"></asp:SqlDataSource>
    <asp:gridview id="gridview1" Cssclass="table" autoGenerateColumns="False" GridLines="None" OnRowCommand="gridview1_RowCommand" PageSize="15" runat="server" DataKeyNames="newsNo" DataSourceID="hangOutnews">
        <Columns>
            <asp:BoundField DataField="newsNo" HeaderText="消息編號" ReadOnly="True" SortExpression="newsNo" />
            <asp:BoundField DataField="admName" HeaderText="發佈者" SortExpression="admNo" />
            <asp:BoundField DataField="newsTitle" HeaderText="標題" SortExpression="newsTitle" />
            <asp:BoundField DataField="newsContent" HeaderText="內容" SortExpression="newsContent" />
            <asp:BoundField DataField="newsDate" HeaderText="發怖日期" SortExpression="newsDate" />
            <asp:CheckBoxField DataField="newsOk" HeaderText="顯示" SortExpression="newsOk" />
            <asp:TemplateField ControlStyle-Font-Names="操作">
                <ItemTemplate>
       <asp:Button runat="server" ID="btnEdit" Text="編輯" CssClass="btn btn-warning" CommandName="Edit" CommandArgument='<%# Eval("newsNo")  %>' />
                <asp:Button runat="server" ID="btnDelete" Text="刪除" CssClass="btn btn-danger" CommandName="Delete" OnClientClick="return confirm('確認刪除消息?')" UseSubmitBehavior="true" CommandArgument='<%#Eval("newsNo") %>' />
                </ItemTemplate>
         
            </asp:TemplateField>
        </Columns>
    </asp:gridview>
</asp:Content>
