﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="member.aspx.cs" Inherits="hangout.member" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
 #main tr:nth-child(2n+1) { background-color:rgba(189, 243, 243,0.4);
  text-align:left;font-weight:bold;font-size:14pt;font-family:微軟正黑體;
        }
  #main tr:nth-child(2n) {
  text-align:left;font-weight:bold;font-size:14pt;font-family:微軟正黑體;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>會員列表</h2>
    <hr />
    <asp:sqldatasource id="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hangOutnews %>" SelectCommand="SELECT * FROM [member]"></asp:sqldatasource>
    <asp:GridView ID="GridView1" runat="server" PageSize="20" CssClass="table" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" OnRowCommand="GridView1_RowCommand" GridLines="None"  BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="memAcc" DataSourceID="SqlDataSource1" ForeColor="Black">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="memNo" HeaderText="會員編號" InsertVisible="False" ReadOnly="True" SortExpression="memNo" />
            <asp:BoundField DataField="memAcc" HeaderText="會員帳號" ReadOnly="True" SortExpression="memAcc" />
            <asp:BoundField DataField="memName" HeaderText="會員名稱" SortExpression="memName" />
            <asp:CheckBoxField DataField="memGender" HeaderText="性別" SortExpression="memGender" />
            <asp:BoundField DataField="memEmail" HeaderText="Email" SortExpression="memEmail" />
            <asp:BoundField DataField="memGrade" HeaderText="聲望值" SortExpression="memGrade" />
            <asp:TemplateField>
               <ItemTemplate>
                    <asp:LinkButton CommandName="info" CommandArgument='<%# Eval("memNo") %>' ID="LinkButton2" type="button" runat="server" class="btn btn-info">資訊</asp:LinkButton>
               </ItemTemplate> 
                      
            </asp:TemplateField>
        </Columns>
        
        <FooterStyle BackColor="#CCCC99" />
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle BackColor="#F7F7DE" />
        <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#FBFBF2" />
        <SortedAscendingHeaderStyle BackColor="#848384" />
        <SortedDescendingCellStyle BackColor="#EAEAD3" />
        <SortedDescendingHeaderStyle BackColor="#575357" />
        </asp:GridView>

      <div class="modal" id="info">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>會員詳細資料
                    </h3>
                </div>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:hangOutnews %>"
                    SelectCommand="select * from member where memNo=@id">
                    <SelectParameters>
                        <asp:Parameter Name="id" Type="String" />
                    </SelectParameters>

                </asp:SqlDataSource>

                <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource2">
                    <ItemTemplate>
                        <table class="table " id="main" border="0">
                            <tr>
                                <td>
                                    會員名稱:  <%# Eval("memName") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    會員帳號:  <%# Eval("memAcc") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    會員性別:  <%# GetGender("memGender") %>
                                </td>
                            </tr>
                                <tr>
                                <td>
                                    會員生日:  <%# Eval("memBirthday") %>
                                </td>

                            </tr>
                                <tr>
                                <td>
                                    身分證字號:  <%# Eval("memId") %>
                                </td>
                            </tr>
                                 <tr>
                                <td>
                                    Email:  <%# Eval("memEmail") %>
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    電話:  <%# Eval("memTel") %>
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    地址:  <%# Eval("memCity") %> <%# Eval("memAdd") %>
                                </td>
                            </tr>
                                <tr>
                                <td>
                                    積分:  <%# Eval("memGrade") %>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>

                <%--<iframe src="member_info.aspx?memNo=1" class="modal-body" runat="server" id="infoframe" style="width:100%;height:680px;"></iframe>--%>
                <div class="modal-footer">
                    <h3>

                    </h3>
                </div>

            </div>
        </div>
    </div>
 

    <script>
        
    </script>
</asp:Content>
