﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="manager_info.aspx.cs" Inherits="hangout.manager_info"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
     <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" /> 
    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" autoGenerateColumns="False" OnRowCommand="GridView1_RowCommand" GridLines="None" DataKeyNames="admNo" PageSize="9" CssClass="table" runat="server" >
                <Columns>
    <asp:BoundField DataField="admNo" HeaderText="管理者編號" ReadOnly="True" SortExpression="admNo" />
            <asp:BoundField DataField="admName" HeaderText="管理者名稱" SortExpression="admName" />
            <asp:BoundField DataField="admAcc" HeaderText="管理者帳號" SortExpression="admAcc" />
                    <asp:TemplateField ControlStyle-Font-Names="操作">
                <ItemTemplate>
               
                <asp:Button runat="server" ID="btnDelete" Text="刪除" CssClass="btn btn-danger" CommandName="deleting" OnClientClick="return confirm('確認刪除會員?')" UseSubmitBehavior="true" CommandArgument='<%#Eval("admNo") %>' />
                   
                </ItemTemplate>
         
            </asp:TemplateField>
                </Columns>
         
            </asp:GridView>
        </div>
    </form>
</body>
</html>
