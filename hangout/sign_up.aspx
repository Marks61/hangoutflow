﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="sign_up.aspx.cs" Inherits="hangout.sign_up" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <h2>註冊管理者帳號</h2>
    <asp:Label ID="Label1"  runat="server" text="" ForeColor="Red" Font-Bold="true" Font-Size="14px"  Style="font-family:微軟正黑體;" ></asp:Label>
    <div class="form-horizontal">
        <hr />
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
       
       
            <div class="form-group">
            <label for="txtAcc" class="control-label col-md-2">管理者帳號</label>
            <div class="col-md-10">
                <asp:TextBox ID="txtAcc" type="text" CssClass="form-control" placeholder="請輸入帳號" runat="server" ValidationGroup="checkaccount"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAcc" Display="Dynamic"
                                             ErrorMessage="(必填)" ForeColor="Red" Font-Size="10pt"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" runat="server" ControlToValidate="txtAcc" ValidationExpression="\S{6,20}"
                            ErrorMessage="(格式不符)" ForeColor="Red" Font-Size="10pt"></asp:RegularExpressionValidator>
                <asp:CustomValidator ID="CustomValidator1" ControlToValidate="txtAcc" runat="server" ErrorMessage="帳號已存在" ValidationGroup="checkaccount" OnServerValidate="CustomValidator1_ServerValidate" ForeColor="red" Font-Size="12px"></asp:CustomValidator>
            </div>
        </div>
            </ContentTemplate>
        </asp:UpdatePanel>

          <div class="form-group">
            <label for="txtPsw" class="control-label col-md-2">管理者密碼</label>
            <div class="col-md-10">
                <asp:TextBox ID="txtPsw" type="password" CssClass="form-control" placeholder="請輸入密碼" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPsw" Display="Dynamic"
                                             ErrorMessage="(必填)" ForeColor="Red" Font-Size="10pt"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPsw" ValidationExpression="\S{6,12}"
                            ErrorMessage="(格式錯誤)" ForeColor="Red" Font-Size="10pt"></asp:RegularExpressionValidator>
            </div>
        </div>
         <div class="form-group">
            <label for="checkPsw" class="control-label col-md-2">請再輸入一次密碼</label>
            <div class="col-md-10">
                <asp:TextBox ID="checkPsw" type="password" MaxLength="12" CssClass="form-control" placeholder="再輸入密碼" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="checkPsw" Display="Dynamic"
                                             ErrorMessage="(必填)" ForeColor="Red" Font-Size="10pt"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" Display="static"  Operator="Equal" ControlToCompare="txtPsw" 
                            ControlToValidate="checkPsw" ErrorMessage="(密碼不相符)" ForeColor="Red" Font-Size="10pt"></asp:CompareValidator>
            </div>
        </div>
         <div class="form-group">
            <label for="txtName" class="control-label col-md-2">管理者姓名</label>
            <div class="col-md-10">
                <asp:TextBox ID="txtName" type="text" CssClass="form-control" placeholder="請輸入姓名" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                             ErrorMessage="(必填)" ForeColor="Red" Font-Size="10pt"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <label for="txtMail" class="control-label col-md-2">信箱</label>
            <div class="col-md-10">
                <asp:TextBox ID="txtMail" type="text" CssClass="form-control" placeholder="請輸入信箱" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtMail"
                                             ErrorMessage="(必填)" ForeColor="Red" Font-Size="10pt"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtMail" ValidationExpression="([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)" runat="server" ErrorMessage="(格式有誤)" ForeColor="Red" Font-Size="10pt"></asp:RegularExpressionValidator>
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-md-2">最高權限</label>
            <div class="col-md-10">
                <asp:CheckBox ID="CheckBox1" runat="server" />
            </div>
        </div>
        </div>
    <div>
              <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="btnSignin" CssClass="btn btn-primary" runat="server" Text="註冊" OnClick="btnSignup_Click"  OnClientClick="return confirm('提醒您，信箱未來將用於重置密碼的驗證。請確認資料避免影響日後的個人權益。')" UseSubmitBehavior="true"/>
                 <asp:Button ID="btncancle" CssClass="btn btn-danger" runat="server" Text="取消" OnClick="btncancle_Click" CausesValidation="false" />
            </div>
        </div>
    </div>
</asp:Content>
