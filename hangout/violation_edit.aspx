﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="violation_edit.aspx.cs" Inherits="hangout.violation_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #ContentPlaceHolder1_photo {
            width:40%;
            height:40%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>詳細資料</h1>
      <div class="form-row form-horizontal">
        <div class="form-group">
            <asp:Label ID="Lable3" for="newsNo" runat="server" Text="違規編號:" Font-Bold="true"></asp:Label>
            <asp:Label ID="newsNo" runat="server" Text=""></asp:Label>
              <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </div>
        
        <hr />
          <div class="form-group">
    <asp:Label ID="Label4" for="txtS" runat="server" Text="檢舉人:" Font-Bold="true" CssClass="control-label"></asp:Label>
        <asp:Label ID="txtS" runat="server" Text=""></asp:Label></div>
                <div class="form-group">
    <asp:Label ID="Label6" for="txtM" runat="server" Text="被檢舉人:" Font-Bold="true" CssClass="control-label"></asp:Label>
        <asp:Label ID="txtM" runat="server" Text=""></asp:Label></div>
<div class="form-group">
    <asp:Label ID="Label8" for="txttitle" runat="server" Text="活動名稱:" Font-Bold="true" CssClass="control-label"></asp:Label>
        <asp:Label ID="txttitle" runat="server" Text=""></asp:Label></div>
        <div class="form-group">
    <asp:Label ID="Label2" for="txtcontent" runat="server" Text="內容:"  Font-Bold="true" CssClass="control-label"></asp:Label>
        <asp:Label ID="txtcontent" runat="server" Text=""></asp:Label></div>
                 <div class="form-group">
    <asp:Label ID="Label9" for="txtimg" runat="server" Text="圖片:"  Font-Bold="true" CssClass="control-label"></asp:Label>
       
                     <img id="photo" runat="server" src="" />
                 </div>
                  <div class="form-group">
    <asp:Label ID="Label7" for="DropDownList1" runat="server" Font-Bold="true" Text="請選擇處理方式:" CssClass="control-label"></asp:Label>
        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="list-group">
<asp:ListItem>請選擇</asp:ListItem>
</asp:DropDownList>
    </div>
          <div class="form-group">
    <asp:Label ID="Label3" for="txtre" runat="server" Font-Bold="true" Text="回覆:" CssClass="control-label"></asp:Label>
        <asp:TextBox ID="txtre" TextMode="MultiLine" placeholder="請輸入內容" Height="200px" runat="server" CssClass="form-control"></asp:TextBox>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtre" runat="server" ErrorMessage="(必填)" ForeColor="Red" Font-Size="12pt"></asp:RequiredFieldValidator>
    </div>
         <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="submit" CssClass="btn btn-primary" runat="server" OnClick="submit_Click" Text="送出" />
                 <asp:Button ID="btncancle" CssClass="btn btn-danger" runat="server" Text="取消" OnClick="btncancle_Click"  CausesValidation="false"  />
            </div>
        </div>
    </div>
</asp:Content>
