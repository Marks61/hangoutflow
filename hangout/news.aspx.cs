﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{
    public partial class news : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["hangOutnews"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void gridview1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string newsNo = e.CommandArgument.ToString();
            if (e.CommandName == "Edit") {
                Response.Redirect("Edit_news.aspx?newsNo=" + newsNo);
            }
            if (e.CommandName == "Delete") {
                SqlCommand del = new SqlCommand("delete from news where newsNo=@id", cdnn);
                del.Parameters.AddWithValue("@id", newsNo.ToString());
                SqlDataAdapter da = new SqlDataAdapter();
                da.DeleteCommand = del;
                cdnn.Open();
                del.ExecuteNonQuery();
                cdnn.Close();
            }
                
            }
      


        protected void hangOutnews_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void btnDelect_Click(object sender, EventArgs e)
        {

        }
    }
}