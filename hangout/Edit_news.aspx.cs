﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{
    public partial class Edit_news : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["hangOutnews"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) { }
            else {
                string id = Request.QueryString["newsNo"];

                SqlCommand news = new SqlCommand("select * from news where newsNo=@id", cdnn);
                news.Parameters.AddWithValue("@id", id.ToString());
                SqlDataReader rd;
                cdnn.Open();
                rd = news.ExecuteReader();
                if (rd.HasRows)
                {
                    while (rd.Read())
                    {
                        bool show = Convert.ToBoolean(rd["newsOk"]);
                        newsNo.Text = id.ToString();
                        txttitle.Text = rd["newsTitle"].ToString();
                        txtcontent.Text = rd["newsContent"].ToString();
                        txtdate.Text = DateTime.Now.ToString();
                        if (show == true)
                        {
                            CheckBox1.Checked = true;
                        }
                        else
                        {
                            CheckBox1.Checked = false;
                        }
                    }
                }
                cdnn.Close();
            }
        }
           

        protected void post_Click(object sender, EventArgs e)
        {
            
            //string date=DateTime.Now.ToString();
            try {
SqlCommand update = new SqlCommand("update news set newsTitle=@title,newsContent=@content,newsDate=@date,newsOk=@bool where newsNo=@id",cdnn);
            update.Parameters.AddWithValue("@id", newsNo.Text);
                update.Parameters.AddWithValue("@title", txttitle.Text);
                update.Parameters.AddWithValue("@content", txtcontent.Text);
                update.Parameters.AddWithValue("@date", Convert.ToDateTime(txtdate.Text));
                if (CheckBox1.Checked)
                {
                    update.Parameters.AddWithValue("@bool", true);
                }
                else
                {
                    update.Parameters.AddWithValue("@bool", false);
                }
                cdnn.Open();
                update.ExecuteNonQuery();
                cdnn.Close();
               

                Response.Redirect("news.aspx");
        
            }
            catch { 
                
            }
            
        }

        protected void btncancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("news.aspx");
        }
    }
}