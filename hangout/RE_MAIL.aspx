﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="RE_MAIL.aspx.cs" Inherits="hangout.RE_MAIL" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>回覆信件
    </h2>
    
        <div class="form-row form-horizontal">
        <div class="form-group">
            <asp:Label ID="Lable3" for="newsNo" runat="server" Text="寄件編號:" Font-Bold="true"></asp:Label>
            <asp:Label ID="newsNo" runat="server" Text=""></asp:Label>
        </div>
        
        <hr />
<div class="form-group">
    <asp:Label ID="Label1" for="txttitle" runat="server" Text="主旨:" Font-Bold="true" CssClass="control-label"></asp:Label>
        <asp:Label ID="txttitle" runat="server" Text="Label"></asp:Label></div>
        <div class="form-group">
    <asp:Label ID="Label2" for="txtcontent" runat="server" Text="內容:"  Font-Bold="true" CssClass="control-label"></asp:Label>
        <asp:Label ID="txtcontent" runat="server" Text="Label"></asp:Label></div>
         <div class="form-group">
    <asp:Label ID="Label3" for="txtdate" runat="server" Font-Bold="true" Text="日期:" CssClass="control-label"></asp:Label>
   
        <asp:Label ID="txtdate" runat="server" Text="Label"></asp:Label>
</div>
         <div class="form-group">
    <asp:Label ID="Label7" for="txtre" runat="server" Font-Bold="true" Text="回覆:" CssClass="control-label"></asp:Label>
        <asp:TextBox ID="txtre" TextMode="MultiLine" placeholder="請輸入內容" Height="200px" runat="server" CssClass="form-control"></asp:TextBox>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtre" runat="server" ErrorMessage="(必填)" ForeColor="Red" Font-Size="12pt"></asp:RequiredFieldValidator>
    </div>
        
         <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="submit" CssClass="btn btn-primary" runat="server" Text="送出" OnClick="submit_Click" />
                 <asp:Button ID="btncancle" CssClass="btn btn-danger" runat="server" Text="取消"  CausesValidation="false" OnClick="btncancle_Click"  />
            </div>
        </div>
    </div>
</asp:Content>
