﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{
    public partial class num : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);


        protected void Page_Load(object sender, EventArgs e)
        {
            var dateing = Convert.ToDateTime(DateTime.Now.ToString());
            if (!IsPostBack)
            {

            
            SqlCommand drop = new SqlCommand("select * from activity where actInDate <= @date", cdnn);
            drop.Parameters.AddWithValue("@date", dateing);
            SqlDataReader drpr;
            cdnn.Open();
            drpr = drop.ExecuteReader();

            ListItem item;
            drop1.Items.Clear();

            while (drpr.Read())
            {
                item = new ListItem(drpr["actName"].ToString(), drpr["actNo"].ToString());
                drop1.Items.Add(item);
            }
            cdnn.Close();
            }
            if (drop1.SelectedValue != "null")
            {
                
            }
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            SqlCommand sear = new SqlCommand("select* from(activitylist inner join member on activitylist.memAcc = member.memAcc) inner join activity on activitylist.actNo = activity.actNo where activity.actNo=@id", cdnn);
            sear.Parameters.AddWithValue("@id", drop1.SelectedValue);
            SqlDataAdapter d = new SqlDataAdapter(sear);
            DataSet dt = new DataSet();
            d.Fill(dt, "Contact");
            GridView1.DataSource = dt;
            GridView1.DataBind();


        }

        protected void drop1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            }

       
            
        

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {   int gra = Convert.ToInt16(Request.Form["num2"]);
                int granuted = -Convert.ToInt16(Request.Form["num2"]);
            if (e.CommandName == "send") {

                //SqlCommand sear2 = new SqlCommand("select* from(activitylist inner join member on activitylist.memAcc = member.memAcc) inner join activity on activitylist.actNo = activity.actNo where activity.actNo=@id", cdnn);
                //sear2.Parameters.AddWithValue("@id", e.CommandArgument);
                //SqlDataReader rd;
                //cdnn.Open();
                //rd = sear2.ExecuteReader();
                //if (rd.HasRows) {
                //    if (rd.Read()) {
                //        Context.Session["check"]=rd["viocheck"];
                //    }
                //}
                    
                //    cdnn.Close();

           
                
                if (gra == 0)
                {
                    Response.Write("<script>alert('聲望值不可以為0')</script>");
                }
                else if (gra >= 0)
                {
                    SqlCommand info = new SqlCommand("select * from activitylist inner join member on activitylist.memNo=member.memNo where listNo=@id", cdnn);
                    info.Parameters.AddWithValue("@id", e.CommandArgument);
                    SqlDataReader rd3;
                    cdnn.Open();
                    rd3 = info.ExecuteReader();
                    if (rd3.HasRows)
                    {
                        if (rd3.Read())
                        {
                            Context.Session["grademem"] = rd3["memNo"];
                            Context.Session["ame"] = rd3["memName"];
                            Context.Session["activity"] = rd3["actName"];
                        }
                    }

                    cdnn.Close();

                    SqlCommand pos = new SqlCommand("update activitylist set actGrade='true' where listNo=@id", cdnn);
                    pos.Parameters.AddWithValue("@id", e.CommandArgument);
                    cdnn.Open();
                    pos.ExecuteNonQuery();
                    cdnn.Close();


                    SqlCommand grade = new SqlCommand("update member set memGrade+=@grade where memNo=@no", cdnn);
                    grade.Parameters.AddWithValue("@no", Session["grademem"]);
                    grade.Parameters.AddWithValue("@grade", gra);
                    cdnn.Open();
                    grade.ExecuteNonQuery();
                    cdnn.Close();

                    string title = "聲望發送通知";
                    string time = DateTime.Now.ToString();
                    string content = "親愛的會員 [" + Session["ame"].ToString() + "]，感謝您參與活動[" + Session["activity"].ToString() + "]。給您的" + gra + "點聲望值已經發送。";
                    SqlCommand cmd = new SqlCommand("insert into mesAdm(memRec,mesAdmSub,mesAdmCon,mesAdmDate,admNo) values(@rec,@title,@content,@date,@account)", cdnn);

                    cmd.Parameters.AddWithValue("@rec", Session["grademem"]);
                    cmd.Parameters.AddWithValue("@title", title.ToString());
                    cmd.Parameters.AddWithValue("@content", content.ToString());
                    cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(time));
                    cmd.Parameters.AddWithValue("@account", Session["Id"]);
                    cdnn.Open();
                    cmd.ExecuteNonQuery();
                    cdnn.Close();

                }
                else if (gra <= 0) {
                    SqlCommand info = new SqlCommand("select * from activitylist inner join member on activitylist.memNo=member.memNo where listNo=@id", cdnn);
                    info.Parameters.AddWithValue("@id", e.CommandArgument);
                    SqlDataReader rd2;
                    cdnn.Open();
                    rd2 = info.ExecuteReader();
                    if (rd2.HasRows)
                    {
                        if (rd2.Read())
                        {
                            Context.Session["grademem"] = rd2["memNo"];
                            Context.Session["ame"] = rd2["memName"];
                            Context.Session["activity"] = rd2["actName"];
                        }
                    }

                    cdnn.Close();

                    SqlCommand pos = new SqlCommand("update activitylist set actGrade='true' where listNo=@id", cdnn);
                    pos.Parameters.AddWithValue("@id", e.CommandArgument);
                    cdnn.Open();
                    pos.ExecuteNonQuery();
                    cdnn.Close();


                    SqlCommand grade = new SqlCommand("update member set memGrade+=@grade where memNo=@no", cdnn);
                    grade.Parameters.AddWithValue("@no", Session["grademem"]);
                    grade.Parameters.AddWithValue("@grade", gra);
                    cdnn.Open();
                    grade.ExecuteNonQuery();
                    cdnn.Close();

                    string title = "聲望異動通知";
                    string time = DateTime.Now.ToString();
                    string content = "親愛的會員 [" + Session["ame"].ToString() + "]，您報名活動[" + Session["activity"].ToString() + "]，但因為點名未到，依據活動規範將刪除您的" + granuted + "點聲望值。";
                    SqlCommand cmd = new SqlCommand("insert into mesAdm(memRec,mesAdmSub,mesAdmCon,mesAdmDate,admNo) values(@rec,@title,@content,@date,@account)", cdnn);

                    cmd.Parameters.AddWithValue("@rec", Session["grademem"]);
                    cmd.Parameters.AddWithValue("@title", title.ToString());
                    cmd.Parameters.AddWithValue("@content", content.ToString());
                    cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(time));
                    cmd.Parameters.AddWithValue("@account", Session["Id"]);
                    cdnn.Open();
                    cmd.ExecuteNonQuery();
                    cdnn.Close();
                    
                }
                
               
                
            }
        }

        //protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        //{

        //}
    }
    }

