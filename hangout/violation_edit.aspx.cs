﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{
    public partial class violation_edit : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);
        SqlConnection cenn = new SqlConnection(ConfigurationManager.ConnectionStrings["Hangoutnews"].ConnectionString);


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) { }
            else
            {
                SqlCommand drop = new SqlCommand("select * from viocode", cdnn);
                SqlDataReader drpr;
                cdnn.Open();
                drpr = drop.ExecuteReader();

                ListItem item;
                DropDownList1.Items.Clear();

                while (drpr.Read())
                {
                    item = new ListItem(drpr["vioLevel"].ToString(), drpr["vioCode"].ToString());
                    DropDownList1.Items.Add(item);
                }
                cdnn.Close();


            }

            string ID = Request.QueryString["vioNo"];
                SqlCommand sql = new SqlCommand("SELECT * FROM  violation INNER JOIN member ON violation.memAcc = member.memAcc where vioNo =@id", cdnn);
                sql.Parameters.AddWithValue("@id", ID);
                SqlDataReader rd;
                cdnn.Open();
                rd = sql.ExecuteReader();
                if (rd.HasRows)
                {
                    while (rd.Read())
                    {
                       
                        newsNo.Text = rd["vioNo"].ToString();
                        txtS.Text = rd["memAcc"].ToString();
                    txttitle.Text = rd["actName"].ToString();
                        photo.Src = "photo.ashx?vioNo=" + newsNo.Text;
                        //Label1.Text = newsNo.Text;
                        txtM.Text = rd["violator"].ToString();
                        txtcontent.Text = rd["vioContent"].ToString();

                    SqlCommand catcj = new SqlCommand("SELECT * FROM member INNER JOIN violation ON violation.violator = member.memAcc where vioNo =@id", cenn);
                    catcj.Parameters.AddWithValue("@id", ID);
                    SqlDataReader r;
                    cenn.Open();
                    r = catcj.ExecuteReader();
                    if(r.HasRows){
                        if (r.Read()) {
Context.Session["memrec"]=r["memNo"];
                        }

                    }
                    
                    cenn.Close();
                    }
                }
                cdnn.Close();

            }

        protected void submit_Click(object sender, EventArgs e)
        {
            try {
    SqlCommand upd1 = new SqlCommand("insert into mesAdm(memRec,mesAdmSub,mesAdmCon,mesAdmDate,admNo) values(@rec,@title,@content,@date,@account)", cdnn);

                string tie = DateTime.Now.ToString();
                upd1.Parameters.AddWithValue("@rec", Session["memrec"]);
                upd1.Parameters.AddWithValue("@title", "違規通知");
                upd1.Parameters.AddWithValue("@content", "親愛的會員"+txtM.Text+"因為違反會員規範。已遭懲處。違規事由:"+txtre.Text);
                upd1.Parameters.AddWithValue("@date", Convert.ToDateTime(tie));
                upd1.Parameters.AddWithValue("@account", Session["Id"]);
            cdnn.Open();
            upd1.ExecuteNonQuery();
            cdnn.Close();
                string ans = DropDownList1.SelectedValue;
                //Response.Write("<script>alert("+ans+")</script>");

                switch (ans) {
                    case "0":
                        break;
                    case "1":
                        SqlCommand info = new SqlCommand("select * from member  where memNo=@id", cdnn);
                        info.Parameters.AddWithValue("@id", Session["memrec"]);
                        SqlDataReader rd;
                        cdnn.Open();
                        rd = info.ExecuteReader();
                        if (rd.HasRows)
                        {
                            if (rd.Read())
                            {
                                Context.Session["grademem"] = rd["memNo"];
                                Context.Session["ame"] = rd["memName"];
                                
                            }
                        }

                        cdnn.Close();

                        


                        SqlCommand grade = new SqlCommand("update member set memGrade-=5 where memNo=@no", cdnn);
                        grade.Parameters.AddWithValue("@no", Session["grademem"]);
                        cdnn.Open();
                        grade.ExecuteNonQuery();
                        cdnn.Close();

                        string title = "聲望異動通知";
                        string time = DateTime.Now.ToString();
                        string content = "親愛的會員 [" + Session["ame"].ToString() + "]，您因為違反會員規範，扣除5點聲望。";
                        SqlCommand cmd = new SqlCommand("insert into mesAdm(memRec,mesAdmSub,mesAdmCon,mesAdmDate,admNo) values(@rec,@title,@content,@date,@account)", cdnn);

                        cmd.Parameters.AddWithValue("@rec", Session["grademem"]);
                        cmd.Parameters.AddWithValue("@title", title.ToString());
                        cmd.Parameters.AddWithValue("@content", content.ToString());
                        cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(time));
                        cmd.Parameters.AddWithValue("@account", Session["Id"]);
                        cdnn.Open();
                        cmd.ExecuteNonQuery();
                        cdnn.Close();
                        break;
                    case "2":
                        SqlCommand info2 = new SqlCommand("select * from member  where memNo=@id", cdnn);
                        info2.Parameters.AddWithValue("@id", Session["memrec"]);
                        SqlDataReader rd2;
                        cdnn.Open();
                        rd2 = info2.ExecuteReader();
                        if (rd2.HasRows)
                        {
                            if (rd2.Read())
                            {
                                Context.Session["grademem"] = rd2["memNo"];
                                Context.Session["ame"] = rd2["memName"];

                            }
                        }

                        cdnn.Close();




                        SqlCommand grade2 = new SqlCommand("update member set memGrade-=10 where memNo=@no", cdnn);
                        grade2.Parameters.AddWithValue("@no", Session["grademem"]);
                        cdnn.Open();
                        grade2.ExecuteNonQuery();
                        cdnn.Close();

                        string title2 = "聲望異動通知";
                        string time2 = DateTime.Now.ToString();
                        string content2 = "親愛的會員 [" + Session["ame"].ToString() + "]，您因為違反會員規範，扣除10點聲望。";
                        SqlCommand cmd2 = new SqlCommand("insert into mesAdm(memRec,mesAdmSub,mesAdmCon,mesAdmDate,admNo) values(@rec,@title,@content,@date,@account)", cdnn);

                        cmd2.Parameters.AddWithValue("@rec", Session["grademem"]);
                        cmd2.Parameters.AddWithValue("@title", title2.ToString());
                        cmd2.Parameters.AddWithValue("@content", content2.ToString());
                        cmd2.Parameters.AddWithValue("@date", Convert.ToDateTime(time2));
                        cmd2.Parameters.AddWithValue("@account", Session["Id"]);
                        cdnn.Open();
                        cmd2.ExecuteNonQuery();
                        cdnn.Close();
                        break;
                    case "3":
                        SqlCommand info3 = new SqlCommand("select * from member  where memNo=@id", cdnn);
                        info3.Parameters.AddWithValue("@id", Session["memrec"]);
                        SqlDataReader rd3;
                        cdnn.Open();
                        rd3 = info3.ExecuteReader();
                        if (rd3.HasRows)
                        {
                            if (rd3.Read())
                            {
                                Context.Session["grademem"] = rd3["memNo"];
                                Context.Session["ame"] = rd3["memName"];

                            }
                        }

                        cdnn.Close();




                        SqlCommand grade3 = new SqlCommand("update member set memGrade-=15 where memNo=@no", cdnn);
                        grade3.Parameters.AddWithValue("@no", Session["grademem"]);
                        cdnn.Open();
                        grade3.ExecuteNonQuery();
                        cdnn.Close();

                        string title3 = "聲望異動通知";
                        string time3 = DateTime.Now.ToString();
                        string content3 = "親愛的會員 [" + Session["ame"].ToString() + "]，您因為違反會員規範，扣除15點聲望。";
                        SqlCommand cmd3 = new SqlCommand("insert into mesAdm(memRec,mesAdmSub,mesAdmCon,mesAdmDate,admNo) values(@rec,@title,@content,@date,@account)", cdnn);

                        cmd3.Parameters.AddWithValue("@rec", Session["grademem"]);
                        cmd3.Parameters.AddWithValue("@title", title3.ToString());
                        cmd3.Parameters.AddWithValue("@content", content3.ToString());
                        cmd3.Parameters.AddWithValue("@date", Convert.ToDateTime(time3));
                        cmd3.Parameters.AddWithValue("@account", Session["Id"]);
                        cdnn.Open();
                        cmd3.ExecuteNonQuery();
                        cdnn.Close();
                        break;
                }

                SqlCommand upd2 = new SqlCommand("update member set memVio=@code where memNo=@rec", cdnn);

               
                upd2.Parameters.AddWithValue("@rec", Session["memrec"]);
                upd2.Parameters.AddWithValue("@code", DropDownList1.SelectedValue);
                cdnn.Open();
                upd2.ExecuteNonQuery();
                cdnn.Close();

                SqlCommand up3 = new SqlCommand("update violation set viocheck='True' where vioNo=@code", cdnn);
                up3.Parameters.AddWithValue("@code", Request.QueryString["vioNo"]);
                cdnn.Open();
                up3.ExecuteNonQuery();
                cdnn.Close();
                Response.Redirect("violation.aspx");
            } catch {

            }
           
        
        }

        protected void btncancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("violation.aspx");
        }
    }
    }