﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{
    public partial class manager_list : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string admNo = e.CommandArgument.ToString();


            if (e.CommandName.ToString() == "Edit")
            {
                while (Session["Id"].ToString() == admNo)
                {
                    Response.Redirect("Edit_manager.aspx?admNo=" + admNo);
                }
                while (Session["Id"].ToString() != admNo)
                {
                    Response.Redirect("manager_list.aspx");
                }
            }

            if (e.CommandName.ToString() == "Deleting")
            {
                if (Session["passer"].ToString() == "True")
                {
                    SqlCommand del = new SqlCommand("delete administrator where admNo=@id", cdnn);
                    del.Parameters.AddWithValue("@id", e.CommandArgument.ToString());
                    cdnn.Open();
                    del.ExecuteNonQuery();
                    cdnn.Close();
                    Response.Redirect("manager_list.aspx");
                }
                else
                {
                    if (Session["Id"].ToString() == admNo)
                    {

                        SqlCommand del = new SqlCommand("delete administrator where admNo=@id", cdnn);
                        del.Parameters.AddWithValue("@id", e.CommandArgument.ToString());
                        cdnn.Open();
                        del.ExecuteNonQuery();
                        cdnn.Close();
                        Response.Redirect("manager_list.aspx");
                    }
                    else if (Session["Id"].ToString() != admNo)
                    {
                        Response.Write("<script>console('失敗。您沒有權限進行這項操作。')</script>");
                        Response.Redirect("manager_list.aspx");

                    }
                }

            }


        }


    }
}
 
