﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{
    public partial class mambaer_info : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            string flag = Request.QueryString["flag"];

            if (!IsPostBack)
            {
                SqlCommand mam = new SqlCommand("select * from member where memName like '%'+@name+'%' ", cdnn);
                string nober = "";
                nober = Request.QueryString["memName"];
                mam.Parameters.AddWithValue("@name", nober);

                SqlDataAdapter dR = new SqlDataAdapter(mam);
                DataSet de = new DataSet();
                dR.Fill(de, "view");
                GridView1.DataSource = de;
                GridView1.DataBind();
            }
        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName == "jump")
            //{
            //    Response.Redirect("mamber_info.aspx?memNo=" + e.CommandArgument.ToString());
            //}

            if (e.CommandName == "info")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                SqlDataSource2.SelectParameters["id"].DefaultValue = i.ToString();

                //ScriptManager.RegisterStartupScript(GetType(), "pop", "$('#info').modal('show')", true);
                ScriptManager.RegisterStartupScript(Page, GetType(), "pop", "$('#info').modal('show')", true);
                //infoframe.Src = "member_info.aspx?memNo=" + e.CommandArgument.ToString();    
            }
        }

        protected string GetGender(string gender)
        {
            if ((bool)Eval(gender))
                return "男";

            return "女";
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            SqlCommand mam = new SqlCommand("select * from member where memName like '%'+@name+'%' ", cdnn);
            string nober = "";
            nober = Request.QueryString["memName"];
            mam.Parameters.AddWithValue("@name", nober);

            SqlDataAdapter dR = new SqlDataAdapter(mam);
            DataSet de = new DataSet();
            dR.Fill(de, "view");
            GridView1.DataSource = de;
            GridView1.DataBind();
        }
    }
}