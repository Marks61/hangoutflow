﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="passter.aspx.cs" Inherits="hangout.passter" %>

<!DOCTYPE html>

<html>
	<head>
		<title>Check In</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />	
        <link href="assets/css/main-sub.css" rel="stylesheet" />
       
	</head>
	<body class="homepage is-preload">
		<div id="page-wrapper">

			<!-- Header -->
				<section id="header" class="wrapper">
                   <!-- Logo -->
<%--						<div id="logo">
							<h1><a href="index.html">HangOut</a></h1>
							<p>一起出去玩</p>
						</div>--%>
                    			
					<!-- Nav -->
						<nav id="nav">
                            <ul>
								<li class="current"><a href="../index.html">首頁</a></li>							
								<li><a href="left-sidebar.html">近期活動</a></li>
								<li><a href="right-sidebar.html">關於HangOut</a></li>
								<li><a href="no-sidebar.html">常見問題</a></li>
                                <li>
									<a href="#">會員專區</a>
									<ul>
                                        <li>
											<a href="#">活動管理</a>
											<ul>
												<li><a href="#">建立活動</a></li>
												<li><a href="#">修改活動</a></li>
												<li><a href="#">刪除活動</a></li>
											</ul>
										</li>										
                                        <li>
											<a href="#">朋友</a>
											<ul>
												<li><a href="#">搜尋會員</a></li>
												<li><a href="#">發送交友邀請</a></li>
												<li><a href="#">朋友清單</a></li>
												<li><a href="#">傳送訊息</a></li>
												<li><a href="#">封鎖</a></li>
												<li><a href="#">刪除</a></li>

											</ul>
										</li>
                                        <li><a href="#">檢舉</a></li>
										<li>
											<a href="#">維護資料</a>
											<ul>
												<li><a href="#">Lorem dolor</a></li>
												<li><a href="#">Amet consequat</a></li>
												<li><a href="#">Magna phasellus</a></li>
												<li><a href="#">Etiam nisl</a></li>
												<li><a href="#">刪除帳號</a></li>
											</ul>
										</li>
										
									</ul>
                                    <li><a href="#">聯繫我們</a></li>
								</li>
							</ul>
						</nav>
				</section>
              </div>

        <!-- 活動簽到 -->
        <header class="style1">
            <h2 style="font-family:'Microsoft JhengHei'">活動簽到</h2>
<%--            <p>點選要簽到的活動</p>--%>
            <asp:Label ID="labtime" CssClass runat="server" Text="Label"></asp:Label>
        </header>
				<section id="highlights" class="wrapper style3">
                   
						<div class="container">
					
						</div>
				
				</section>
        			<!-- Footer -->
				<section id="footer" class="wrapper">
				
						<div id="copyright">
							<ul>
								<li>&copy; Untitled.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
							</ul>
						</div>
				
				</section>

		

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
</body>
</html>
