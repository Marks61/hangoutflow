﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace hangout
{
    public partial class violation : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["Hangoutnews"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
           

        }

        protected void gridview1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "info")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                SqlDataSource2.SelectParameters["id"].DefaultValue = i.ToString();

                //ScriptManager.RegisterStartupScript(GetType(), "pop", "$('#info').modal('show')", true);
                ScriptManager.RegisterStartupScript(Page, GetType(), "pop", "$('#info').modal('show')", true);
                //infoframe.Src = "member_info.aspx?memNo=" + e.CommandArgument.ToString();    
            }
            if (e.CommandName == "work")
            {
                SqlCommand che = new SqlCommand("select * from violation where vioNo=@list", cdnn);
                che.Parameters.AddWithValue("@list", e.CommandArgument.ToString());
                SqlDataReader rd;
                cdnn.Open();
                rd = che.ExecuteReader();
                if (rd.HasRows) {
                    if (rd.Read()) {
                        Context.Session["checkk"] = rd["vioCheck"].ToString();
                    }
                }
                cdnn.Close();

                if (Session["checkk"].ToString() == "True") {
                    Response.Write("<script>alert('違規事件已經處理!!')</script>");
                }else if(Session["checkk"].ToString() != "True")
                {
                    Response.Redirect("violation_edit.aspx?vioNo=" + e.CommandArgument.ToString());  
                }
                
            }
        }
    }
}