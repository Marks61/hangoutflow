﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace hangout
{
    
    public partial class check_in : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            

        }
            protected void Button1_Click(object sender, EventArgs e)
        {
            


        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            
            
        SqlCommand load = new SqlCommand("select * from (activitylist inner join member on activitylist.memAcc=member.memAcc) inner join activity on activitylist.actNo=activity.actNo where activity.actNo=@id",cdnn);
            load.Parameters.AddWithValue("@id", DropDownList1.SelectedValue);
            SqlDataAdapter d = new SqlDataAdapter(load);
            DataSet dt = new DataSet();
            d.Fill(dt, "Contact");
            GridView1.DataSource = dt;
           
          
                GridView1.DataBind();
           
            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (IsPostBack) { 
                SqlCommand drop = new SqlCommand("select * from activity where actInDate=@date order by actNo desc", cdnn);
            drop.Parameters.AddWithValue("@date", startdate.Text + " " + starttime.Text);
            SqlDataReader drpr;
            cdnn.Open();
            drpr = drop.ExecuteReader();

            ListItem item;
            DropDownList1.Items.Clear();

            while (drpr.Read())
            {
                item = new ListItem(drpr["actName"].ToString(), drpr["actNo"].ToString());
                DropDownList1.Items.Add(item);
            }
            cdnn.Close();
        }

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "pass")
            {
                SqlCommand pas = new SqlCommand("update activitylist set activitysignup=@bool where listNo=@id", cdnn);
                pas.Parameters.AddWithValue("@id", e.CommandArgument.ToString());
                pas.Parameters.AddWithValue("@bool", true);
                cdnn.Open();
                pas.ExecuteNonQuery();
                cdnn.Close();
              
            }
            if (e.CommandName == "delay")
            {
                SqlCommand pas = new SqlCommand("update activitylist set activitysignup=@bool where listNo=@id", cdnn);
                pas.Parameters.AddWithValue("@id", e.CommandArgument.ToString());
                pas.Parameters.AddWithValue("@bool", false);
                cdnn.Open();
                pas.ExecuteNonQuery();
                cdnn.Close();

            }
        }
    }
}