﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{
    public partial class manager_info : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            string flag = Request.QueryString["flag"];

            if (!IsPostBack)
            {
                SqlCommand mam = new SqlCommand("select * from administrator where admName like '%'+@name+'%' ", cdnn);
                string nober = "";
                nober = Request.QueryString["admName"];
                mam.Parameters.AddWithValue("@name", nober);

                SqlDataAdapter dR = new SqlDataAdapter(mam);
                DataSet de = new DataSet();
                dR.Fill(de, "view");
                GridView1.DataSource = de;
                GridView1.DataBind();
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string admNo = e.CommandArgument.ToString();
            string pass= Request.QueryString["pass"];

            if (e.CommandName.ToString() == "deleting" && pass =="True")
                {

                    SqlCommand del = new SqlCommand("delete administrator where admNo=@id", cdnn);
                    del.Parameters.AddWithValue("@id", e.CommandArgument.ToString());
                    cdnn.Open();
                    del.ExecuteNonQuery();
                    cdnn.Close();
                Response.Redirect("manager_info.aspx?admName=&flag=1&pass="+Session["passer"].ToString());
            }
                else
                {
               Response.Write("<script>console.log('失敗。您沒有權限進行這項操作。')</script>");
                //Response.Redirect("search.aspx");

                }

            


        }
    }
}
