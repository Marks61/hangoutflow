﻿//------------------------------------------------------------------------------
// <自動產生的>
//     這段程式碼是由工具產生的。
//
//     變更這個檔案可能會導致不正確的行為，而且如果已重新產生
//     程式碼，則會遺失變更。
// </自動產生的>
//------------------------------------------------------------------------------

namespace hangout {
    
    
    public partial class rebootPsw {
        
        /// <summary>
        /// txtPsw 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtPsw;
        
        /// <summary>
        /// RequiredFieldValidator3 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator3;
        
        /// <summary>
        /// RegularExpressionValidator2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator2;
        
        /// <summary>
        /// checkPsw 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox checkPsw;
        
        /// <summary>
        /// RequiredFieldValidator4 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator4;
        
        /// <summary>
        /// CompareValidator1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CompareValidator CompareValidator1;
        
        /// <summary>
        /// post 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button post;
        
        /// <summary>
        /// cancel 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton cancel;
    }
}
