﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Edit_news.aspx.cs" Inherits="hangout.Edit_news" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>修改最新消息</h2>
    <div class="form-row form-horizontal">
        <div class="form-group">
            <asp:Label ID="Lable3" for="newsNo" runat="server" Text="消息編號:" Font-Bold="true"></asp:Label>
            <asp:Label ID="newsNo" runat="server" Text=""></asp:Label>
        </div>
        
        <hr />
<div class="form-group">
    <asp:Label ID="Label1" for="txttitle" runat="server" Text="主旨" Font-Bold="true" CssClass="control-label col-md-2"></asp:Label>
    <div class="col-md-10">
 <asp:TextBox ID="txttitle" runat="server" CssClass="form-control"  Text="" placeholder="請輸入主旨"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txttitle" runat="server" ErrorMessage="(必填)" ForeColor="Red" Font-Size="12pt"></asp:RequiredFieldValidator>
    </div>
</div>
        <div class="form-group">
    <asp:Label ID="Label2" for="txtcontent" runat="server" Text="內容"  Font-Bold="true" CssClass="control-label col-md-2"></asp:Label>
    <div class="col-md-10">
 <asp:TextBox ID="txtcontent" TextMode="MultiLine" placeholder="請輸入內容" Height="200px" runat="server" CssClass="form-control"></asp:TextBox>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtcontent" runat="server" ErrorMessage="(必填)" ForeColor="Red" Font-Size="12pt"></asp:RequiredFieldValidator>
    </div>
</div>
         <div class="form-group">
    <asp:Label ID="Label3" for="txtdate" runat="server" Font-Bold="true" Text="文章日期" CssClass="control-label col-md-2"></asp:Label>
    <div class="col-md-10">
 <asp:TextBox ID="txtdate" TextMode="DateTime" placeholder="yy/mm/dd" Font-Bold="true"  runat="server" CssClass="form-control"></asp:TextBox>
    </div>
</div>
         <div class="form-group">
            <label class="control-label col-md-2">是否顯示</label>
            <div class="col-md-10">
                <asp:CheckBox ID="CheckBox1"  runat="server" />
            </div>
        </div>
         <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="submit" CssClass="btn btn-primary" runat="server" Text="送出" OnClick="post_Click" />
                 <asp:Button ID="btncancle" CssClass="btn btn-danger" runat="server" Text="取消"  CausesValidation="false" OnClick="btncancle_Click" />
            </div>
        </div>
    </div>
   
    
</asp:Content>

