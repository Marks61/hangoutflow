﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="activity.aspx.cs" Inherits="hangout.activity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <style>
 #main tr:nth-child(2n+1) { background-color:rgba(189, 243, 243,0.4);
  text-align:left;font-weight:bold;font-size:14pt;font-family:微軟正黑體;
        }
  #main tr:nth-child(2n) {
  text-align:left;font-weight:bold;font-size:14pt;font-family:微軟正黑體;
        }

   #board tr:nth-child(2n+1) { background-color:#FFDDAA;
  text-align:left;font-weight:bold;font-size:14pt;font-family:微軟正黑體;
        }
  #board tr:nth-child(2n) {background-color:#FFC8B4	;
  text-align:left;font-weight:bold;font-size:14pt;font-family:微軟正黑體;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h2>活動列表</h2>
    <hr />

    <asp:GridView ID="GridView1" runat="server" PageSize="20" CssClass="table" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" OnRowCommand="GridView1_RowCommand" GridLines="None"  BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="actNo" ForeColor="Black">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="actNo" HeaderText="活動編號" InsertVisible="False" ReadOnly="True" SortExpression="memNo" />
            <asp:BoundField DataField="memAcc" HeaderText="會員帳號" ReadOnly="True" SortExpression="memAcc" />
            <asp:BoundField DataField="actName" HeaderText="活動名稱" SortExpression="memName" />
            <asp:BoundField DataField="actCidname" HeaderText="活動類型" SortExpression="memGrade" />
            <asp:CheckBoxField DataField="actOk" HeaderText="活動是否成立?" SortExpression="memGender" />
            <asp:BoundField DataField="actDeadline" HeaderText="報名截止時間" SortExpression="memEmail" />
            <asp:BoundField DataField="actInDate" HeaderText="活動開始時間" SortExpression="memGrade" />
            <asp:TemplateField>
               <ItemTemplate>
                    <asp:LinkButton CommandName="info" CommandArgument='<%# Eval("actNo") %>' ID="LinkButton2" type="button" runat="server" class="btn btn-info">資訊</asp:LinkButton>
                    <asp:LinkButton CommandName="message" CommandArgument='<%# Eval("actNo") %>' ID="LinkButton1" type="button" runat="server" class="btn btn-info">留言板</asp:LinkButton><br />
                  <asp:Button runat="server" ID="Button1" Text="取消活動" CssClass="btn btn-danger" CommandName="canceling" CommandArgument='<%# Eval("actNo")  %>'
                      OnClientClick="return confirm('確認關閉活動?')" UseSubmitBehavior="true" />
                   <asp:Button runat="server" ID="Button2" Text="開啟活動" CssClass="btn btn-warning" CommandName="opening" CommandArgument='<%# Eval("actNo")  %>'
                       OnClientClick="return confirm('確認開啟活動?')" UseSubmitBehavior="true" />
               </ItemTemplate> 
                      
            </asp:TemplateField>
        </Columns>
        
        <FooterStyle BackColor="#CCCC99" />
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle BackColor="#F7F7DE" />
        <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#FBFBF2" />
        <SortedAscendingHeaderStyle BackColor="#848384" />
        <SortedDescendingCellStyle BackColor="#EAEAD3" />
        <SortedDescendingHeaderStyle BackColor="#575357" />
        </asp:GridView>

      <div class="modal" id="info">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>活動詳細資料
                    </h3>
              </div>
              <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:hangOutnews %>"
                    SelectCommand="SELECT * FROM  activity INNER JOIN member ON activity.memAcc = member.memAcc where actNo=@id">
                    <SelectParameters>
                        <asp:Parameter Name="id" Type="String" />
                    </SelectParameters>

    

                </asp:SqlDataSource>

                <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource2">
                    <ItemTemplate>
                        <table class="table " id="main" border="0">
                            <tr>
                                <td>
                                    會員名稱:  <%# Eval("memName") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    活動類型:  <%# Eval("actCidname") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                        活動名稱:  <%# Eval("actName") %>
                                </td>
                            </tr>
                                <tr>
                                <td>
                                    地址:  <%# Eval("actLoc") %> <%# Eval("actAdd") %>
                                </td>

                            </tr>
                                <tr>
                                <td>
                                    活動日期:  <%# Eval("actInDate") %>~ <%# Eval("actExDate") %>
                                </td>
                            </tr>
                                 <tr>
                                <td>
                                    報名截止日期:  <%# Eval("actDeadline") %>
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    活動內容:  <%# Eval("actContent") %>
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    年齡限制:<%# Eval("actAge") %>
                                </td>
                            </tr>
                                <tr>
                                <td>
                                    人數限制:  <%# Eval("actPeoNum") %>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    需要費用?:  <%# Getfree("actFee") %>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    是否公開:?:  <%# GetPublic("actPublic") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    是否成團?:  <%# Getopen("actOk") %>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>

               <%-- <iframe src="member_info.aspx?memNo=1" class="modal-body" runat="server" id="infoframe" style="width:100%;height:680px;"></iframe>--%>
                <div class="modal-footer">
                    <h3>

                    </h3>
                </div>

            </div>
        </div>
    </div>

     <div class="modal" id="messager" >
        <div class="modal-dialog">
            <div class="modal-content"  style="width:120%;margin-left:-7%">
                <div class="modal-header">
                    <h3>活動留言板
                    </h3>
              </div>
              
                  <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:hangOutnews %>"
                    SelectCommand="select * from (activitybord inner join member on activitybord.memNo=member.memNo) inner join activity on activity.actName=activitybord.actName where activitybord.actNo=@id">
                    <SelectParameters>
                        <asp:Parameter Name="id" Type="String" />
                    </SelectParameters>

    

                </asp:SqlDataSource><table class="table" id="board" border="0" >
              <asp:Repeater ID="Repeater2" runat="server"  DataSourceID="SqlDataSource3">
               
                    

                  
                    
         
                <ItemTemplate>
                    
                    <tr>
                        <td>
                            <%# Eval("memName") %>
                        </td>
                        <td>
                            <%# Eval("actContent") %>
                        </td>
                        <td>
                            <%# Eval("actBdDate") %>
                        </td>
                    </tr>

                   
                </ItemTemplate>
           </asp:Repeater>
 </table>
               <%-- <iframe src="member_info.aspx?memNo=1" class="modal-body" runat="server" id="infoframe" style="width:100%;height:680px;"></iframe>--%>
                <div class="modal-footer">
                    <h3>

                    </h3>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
