﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{
    public partial class Edit_manager : System.Web.UI.Page
    { 
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            //讀取管理者資訊，套用到介面
            if (IsPostBack) { }
            else
            {
                string nober = "";
                nober = Request.QueryString["admNo"];
                SqlCommand edm = new SqlCommand("select * from administrator where admNo=@no", cdnn);
                edm.Parameters.AddWithValue("@no", nober);
                SqlDataReader rd;
                cdnn.Open();
                rd = edm.ExecuteReader();
                if (rd.HasRows)
                {
                    while (rd.Read())
                    {
                        txtName.Text= rd["admName"].ToString();
                        txtMail.Text = rd["admMail"].ToString();
                        if (Convert.ToBoolean(rd["admHighest"]) == true)
                        {
                            CheckBox1.Checked = true;
                        }
                        else
                        {
                            CheckBox1.Checked = false;
                        }

                    }
                }
                cdnn.Close();
            }
        }

        protected void submit_Click(object sender, EventArgs e)
        { 
            try
            {
                SqlCommand edit = new SqlCommand("update administrator set admAcc=@account,admPsw=@psw,admName=@name,admMail=@mail where admNo=@id", cdnn);
                edit.Parameters.AddWithValue("@id",Session["Id"].ToString());
                edit.Parameters.AddWithValue("@account", Session["Account"].ToString());
                edit.Parameters.AddWithValue("@psw", Session["password"].ToString());
                edit.Parameters.AddWithValue("@name", txtName.Text);

                //1.建毅，註銷這一段
                if (CheckBox1.Checked == true)
                {
                    edit.Parameters.AddWithValue("@paster", true);
                }
                else if (CheckBox1.Checked == false)
                {
                    edit.Parameters.AddWithValue("@paster", false);
                }


                edit.Parameters.AddWithValue("@mail", txtMail.Text);
                cdnn.Open();
                edit.ExecuteNonQuery();
                cdnn.Close();
                Response.Redirect("manager_list.aspx");
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
        protected void btnchangPsw_Click(object sender, EventArgs e)
        {
            Response.Redirect("rebootPsw.aspx?account=" + Session["Account"].ToString());
        }

        protected void btncancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("manager_list.aspx");
        }
    }
}