﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Net.Http;

namespace hangout
{
    public partial class rebootmail : System.Web.UI.Page
    {
        SqlConnection cdnn =new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlCommand search = new SqlCommand("select * from administrator where admAcc=@account",cdnn);
            search.Parameters.AddWithValue("@account", Session["account"].ToString());
           
            SqlDataReader rd;
            cdnn.Open();
            rd = search.ExecuteReader();
            if (rd.HasRows) {
                while (rd.Read()) {
                    Context.Session["mail"]=rd["admMail"];
                    Context.Session["name"] = rd["admName"];
                }
            }
            cdnn.Close();
          

            SmtpClient mymail = new SmtpClient("smtp.gmail.com", 587);
            mymail.Credentials = new NetworkCredential("hangout10801", "mcd10801");
            mymail.EnableSsl = true;
            string from = "hangout10801@gmail.com";
            string to = Session["mail"].ToString();
            string subject ="忘記密碼驗證信" ;
            var body = "管理員 [" + Session["name"].ToString() + "] 您已提出重置密碼的申請。請點擊網址繼續接下來的程序 http://10.10.3.102/rebootPsw.aspx?account="
                + Session["account"].ToString();
            mymail.Send(from, to, subject,body);
        }
    }
}