﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="violation.aspx.cs" Inherits="hangout.violation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style>
 #main tr:nth-child(2n+1) { background-color:rgba(189, 243, 243,0.4);
  text-align:left;font-weight:bold;font-size:14pt;font-family:微軟正黑體;
        }
  #main tr:nth-child(2n) {
  text-align:left;font-weight:bold;font-size:14pt;font-family:微軟正黑體;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>違規紀錄</h2>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hangOutnews %>" SelectCommand="SELECT * FROM [violation]"></asp:SqlDataSource>
    <asp:gridview id="gridview1" runat="server" OnRowCommand="gridview1_RowCommand" GridLines="None"  BorderStyle="None" CssClass="table" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderWidth="1px" CellPadding="3" DataKeyNames="vioNo" DataSourceID="SqlDataSource1" ForeColor="Black">
        <AlternatingRowStyle BackColor="#CCCCCC" />
        <Columns>
            <asp:BoundField DataField="vioNo" HeaderText="違規舉告編號" InsertVisible="False" ReadOnly="True" SortExpression="vioNo" />
            <asp:BoundField DataField="memAcc" HeaderText="檢舉人" SortExpression="memAcc" />
            <asp:BoundField DataField="vioType" HeaderText="舉告類型" SortExpression="vioType" />
            <asp:BoundField DataField="violator" HeaderText="被檢舉人" SortExpression="violator" />
            <asp:TemplateField>
           <ItemTemplate>
                    <asp:LinkButton CommandName="info" CommandArgument='<%# Eval("vioNo") %>' ID="LinkButton2" type="button" runat="server" class="btn btn-info">資訊</asp:LinkButton>
                  <asp:Button runat="server" ID="Button1" Text="處理違規" CssClass="btn btn-danger" CommandName="work" CommandArgument='<%# Eval("vioNo")  %>' />
               </ItemTemplate> 

            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />

    </asp:gridview>
          <div class="modal" id="info">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>違規詳細資料
                    </h3>
              </div>
              <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:hangOutnews %>"
                    SelectCommand="SELECT * FROM [violation] where vioNo=@id">
                    <SelectParameters>
                        <asp:Parameter Name="id" Type="String" />
                    </SelectParameters>

                </asp:SqlDataSource>

                <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource2">
                    <ItemTemplate>
                        <table class="table " id="main" border="0">
                            <tr>
                                <td>
                                    檢舉人:  <%# Eval("memAcc") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    類型:  <%# Eval("vioType") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                        被檢舉人:  <%# Eval("violator") %>
                                </td>
                            </tr>
                               
                                <tr>
                                <td>
                                    檢舉事由:  <%# Eval("vioContent") %>
                                </td>
                            </tr>
                              
                        </table>
                    </ItemTemplate>
                </asp:Repeater>

               <%-- <iframe src="member_info.aspx?memNo=1" class="modal-body" runat="server" id="infoframe" style="width:100%;height:680px;"></iframe>--%>
                <div class="modal-footer">
                    <h3>

                    </h3>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
