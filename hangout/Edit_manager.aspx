﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Edit_manager.aspx.cs" Inherits="hangout.Edit_manager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h2>修改個人資料</h2>
       <div class="form-horizontal">
        <hr />
         <div class="form-group">
            <label for="txtName" class="control-label col-md-2">管理者姓名</label>
            <div class="col-md-10">
                <asp:TextBox ID="txtName" type="text" CssClass="form-control" placeholder="請輸入姓名" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                             ErrorMessage="(必填)" ForeColor="Red" Font-Size="10pt"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <label for="txtMail" class="control-label col-md-2">信箱</label>
            <div class="col-md-10">
                <asp:TextBox ID="txtMail" type="text" CssClass="form-control" placeholder="請輸入信箱" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtMail"
                                             ErrorMessage="(必填)" ForeColor="Red" Font-Size="10pt"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtMail" ValidationExpression="([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)" runat="server" ErrorMessage="(格式有誤)" ForeColor="Red" Font-Size="10pt"></asp:RegularExpressionValidator>
            </div>
        </div>
          <%-- 2.建毅，將checkbox1套用readonly--%>
         <div class="form-group">
            <label class="control-label col-md-2">最高權限</label>
            <div class="col-md-10">
                <asp:CheckBox ID="CheckBox1" onclick="return false" runat="server" />
            </div>
        </div>
        </div>

    <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="submit" CssClass="btn btn-primary" runat="server" Text="送出" OnClick="submit_Click" />
                 <asp:Button ID="btncancle" CssClass="btn btn-danger" runat="server" Text="取消" OnClick="btncancle_Click"  CausesValidation="false" />
                <asp:Button ID="btnchangPsw" CssClass="btn btn-info" runat="server" Text="重設密碼" OnClick="btnchangPsw_Click" CausesValidation="false" />
            </div>
        </div>
</asp:Content>
