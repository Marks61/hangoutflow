﻿<%@ Page Title="" Language="C#" MasterPageFile="~/forgot.Master" AutoEventWireup="true" CodeBehind="rebootPsw.aspx.cs" Inherits="hangout.rebootPsw" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <h2 class="control-label col-md-5">* 請輸入密碼 *</h2>
     <div class="form-horizontal">
 <div class="form-group">
            <label for="id" class="control-label col-md-2">管理者密碼</label>
            <div class="col-md-10">
                <asp:TextBox ID="txtPsw" type="password" CssClass="form-control" placeholder="請輸入密碼" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPsw" Display="Dynamic"
                                             ErrorMessage="(必填)" ForeColor="Red" Font-Size="10pt"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPsw" ValidationExpression="\S{6,12}"
                            ErrorMessage="(格式錯誤)" ForeColor="Red" Font-Size="10pt"></asp:RegularExpressionValidator>
            </div>
        </div>
         <div class="form-group">
            <label for="id" class="control-label col-md-2">請再輸入一次密碼</label>
            <div class="col-md-10">
                <asp:TextBox ID="checkPsw" type="password" MaxLength="12" CssClass="form-control" placeholder="再輸入密碼" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="checkPsw" Display="Dynamic"
                                             ErrorMessage="(必填)" ForeColor="Red" Font-Size="10pt"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" Display="static"  Operator="Equal" ControlToCompare="txtPsw" 
                            ControlToValidate="checkPsw" ErrorMessage="(密碼不相符)" ForeColor="Red" Font-Size="10pt"></asp:CompareValidator>
            </div>
        </div>
         
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="post" type="submit" runat="server" Text="送出" CssClass="btn btn-dark" onclick="post_Click"/>
                </div>
            </div>
     <div  style="text-align:center;font-size:16pt;margin-top:450px;"><span id="motion">步驟 3/4 </span><asp:LinkButton ID="cancel" runat="server" CausesValidation="false"><a id="back" href="login.aspx" >取消</a></asp:LinkButton></div>
<%--    <script>
       motion.innerText = "步驟 2/3 ";
    </script>--%>
</asp:Content>
