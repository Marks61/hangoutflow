﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;

namespace hangout.App_Code
{
    public class pass
    {
       // string word;                             //取得來源字串
        //string returnPsw;  //輸出
        //public string word;

        public string Change(string Word)
        {
            //string Word = word;
            
            SHA512 sha512 = new SHA512CryptoServiceProvider();  //建立一個SHA512
            byte[] source = Encoding.Default.GetBytes(Word);   //將字串轉為Byte[]
            byte[] crypto = sha512.ComputeHash(source);         //進行SHA512加密
                                                                //把加密後的字串從Byte[]轉為字串
            return Convert.ToBase64String(crypto);
        }            

    }
}