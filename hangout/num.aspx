﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="num.aspx.cs" Inherits="hangout.num" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        table thead tr {
            background-color:darkblue;
            color:white;
            border:1px solid white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>聲望管理中心<small style="color:red">警告:聲望值操作將影響會員權利，聲望值發送後不接受修改，請謹慎操作。</small></h2>
    <div class="row">
<div class="form-group">
        <asp:Label ID="Label1" CssClass="col-lg-2" ForeColor="ForestGreen" for="drop" runat="server" Text="活動名稱:" Font-Bold="true"></asp:Label>
        <asp:DropDownList ID="drop1" CssClass="col-lg-4" AutoPostBack="true" OnSelectedIndexChanged="drop1_SelectedIndexChanged" runat="server">
        </asp:DropDownList>
        <asp:Button ID="Button2" align="right" Style="margin-left:98%" CssClass="btn btn-info" runat="server" OnClick="Button2_Click" Text="送出查詢" />
    </div>
    </div>
    
    <asp:GridView ID="GridView1" runat="server" autoGenerateColumns="False" GridLines="None" PageSize="15" OnRowCommand="GridView1_RowCommand"  DataKeyNames="listNo" CssClass="table">
         <Columns>
            <asp:BoundField DataField="listNo" HeaderText="編號" InsertVisible="False" ReadOnly="True" SortExpression="contactNo" />
            <asp:BoundField DataField="actName" HeaderText="活動名稱" SortExpression="memNo" />
            <asp:BoundField DataField="memName" HeaderText="會員" SortExpression="contactTitle" />
            <asp:BoundField DataField="memAcc" HeaderText="會員帳號" SortExpression="contactText" />
            <asp:BoundField DataField="activitysignup" HeaderText="簽到" DataFormatString='{0:}' />
             <asp:TemplateField>
               <ItemTemplate>
                   <%--<asp:TextBox ID="value_change" runat="server" Text="0"></asp:TextBox>--%>
<%--                   <input id="valuechange" name="num" value="0" type="Text" />--%>
                   <input id="valuechange" name="num2" value="0" type="text" />
<%--                   <asp:Button ID="Button1" runat="server" Text="+" />
                   <asp:Button ID="Button3" runat="server" Text="-" />--%>
                   <input id="muted" type="button" value="-" />
                   <input id="plus" type="button" value="+" />
<%--                   <input id="muted" type="button" value="-" />--%>
<%--                   <input id="plus" type="button" value="+" />--%>
                   <asp:Button ID="post" CssClass="btn btn-info" runat="server" Text="送出" CommandName="send" CommandArgument='<%# Eval("listNo")%>' />
  
    <script>
        Number(valuechange.value);
        $("#plus").click(function () {

            valuechange.value++;

        });
        $("#muted").click(function () {

            valuechange.value--;
        });
    </script>
               </ItemTemplate> 
                
            </asp:TemplateField>
             <asp:CheckBoxField DataField="actGrade" HeaderText="送出聲望值" />
        </Columns>
    </asp:GridView>
 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

</asp:Content>
