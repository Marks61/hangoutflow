﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="create_edit.aspx.cs" Inherits="hangout.create_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>新增會員訊息</h2>
     <asp:Label ID="Label1" runat="server" Text="" Font-Bold="true" ForeColor="Red"></asp:Label>
    <hr />
    <div class="form-horizontal">
         <div class="form-group">
               <asp:Label ID="Label7" for="DropDownList1" Cssclass="control-label col-md-2" runat="server" Font-Bold="true" Text="收信者:" ></asp:Label>
        <asp:DropDownList ID="DropDownList1" Cssclass="list-group col-md-8" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
<asp:ListItem>請選擇</asp:ListItem>
</asp:DropDownList>
             <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
        </div>
         <div class="form-group">
            <asp:label runat="server" Cssclass="control-label col-md-2" for="txtTitle" text="標題:" font-Bold="true"></asp:label>
            <asp:textbox ID="txtTitle" Cssclass="form-control col-md-10" runat="server"></asp:textbox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTitle" ErrorMessage="(必填)" ForeColor="Red" Font-Size="12pt"></asp:RequiredFieldValidator>

        </div>
         <div class="form-group">
            <asp:label runat="server" Cssclass="control-label col-md-2" for="txtContent" text="內容:" font-Bold="true"></asp:label>
            <asp:textbox ID="txtContent" Cssclass="form-control col-md-10" TextMode="MultiLine" Height="200px" runat="server"></asp:textbox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtContent" ErrorMessage="(必填)" ForeColor="Red" Font-Size="12pt"></asp:RequiredFieldValidator>
        </div>
               <div class="form-group">
            <asp:label runat="server" Cssclass="control-label col-md-2" for="FileUpload1" text="圖片上傳:" font-Bold="true"></asp:label>
                   <asp:FileUpload ID="FileUpload1" CssClass="control-label col-md-10" runat="server" />
        </div>
<%--      <div id="img_s"></div>--%>

        <div class="form-group">
            <asp:label runat="server" Cssclass="control-label col-md-2" for="txtDate" text="發布日期:" font-Bold="true"></asp:label>
            <asp:textbox ID="txtDate" Cssclass="form-control col-md-10" readOnly="true" runat="server"></asp:textbox>
        </div>
       
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="submit" CssClass="btn btn-primary" runat="server" Text="送出" OnClick="submit_Click"/>
                 <asp:Button ID="btncancle" CssClass="btn btn-danger" runat="server" Text="取消" OnClick="btncancle_Click"  CausesValidation="false" />
            </div>
        </div>
        </div>
  <%--  </div>
    <script>
  function load_img(p_src,write_id){
var t_html;
if(p_src!=''){
if(write_id==img_s){
t_html="<img src='"+p_src+"' onLoad='javascript:if(this.width>120){this.width=120;}if(this.height>120){this.height=120;}'>";
}else{
t_html="<img src='"+p_src+"' onLoad='javascript:if(this.width>240){this.width=240;}if(this.height>240){this.height=240;}'>";
}
}else{
t_html="";
}
write_id.innerHTML=t_html;
}
    </script>--%>
</asp:Content>
