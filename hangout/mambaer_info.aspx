﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="mambaer_info.aspx.cs" Inherits="hangout.mambaer_info" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
     <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" /> 
    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
              <div>
            <asp:GridView ID="GridView1" autoGenerateColumns="False" OnRowCommand="GridView1_RowCommand"  GridLines="None" DataKeyNames="memNo" allowpaging="true" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="8" CssClass="table" runat="server" >
                <Columns>
    <asp:BoundField DataField="memNo" HeaderText="會員編號" ReadOnly="True" SortExpression="memNo" />
            <asp:BoundField DataField="memName" HeaderText="會員名稱" SortExpression="memName" />
            <asp:BoundField DataField="memAcc" HeaderText="會員帳號" SortExpression="memAcc" />
                    <asp:TemplateField>
               <ItemTemplate>
                    <asp:LinkButton CommandName="info" CommandArgument='<%# Eval("memNo") %>' ID="LinkButton2" type="button" runat="server" class="btn btn-info">資訊</asp:LinkButton>
               </ItemTemplate> 
                      
            </asp:TemplateField>
                </Columns>
         
            </asp:GridView>

                        <div class="modal" id="info">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>會員詳細資料
                    </h3>
                </div>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:hangOutnews %>"
                    SelectCommand="select * from member where memNo=@id">
                    <SelectParameters>
                        <asp:Parameter Name="id" Type="String" />
                    </SelectParameters>

                </asp:SqlDataSource>

                <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource2">
                    <ItemTemplate>
                        <table class="table " id="main" border="0">
                            <tr>
                                <td>
                                    會員名稱:  <%# Eval("memName") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    會員帳號:  <%# Eval("memAcc") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    會員性別:  <%# GetGender("memGender") %>
                                </td>
                            </tr>
                                <tr>
                                <td>
                                    會員生日:  <%# Eval("memBirthday") %>
                                </td>

                            </tr>
                                <tr>
                                <td>
                                    身分證字號:  <%# Eval("memId") %>
                                </td>
                            </tr>
                                 <tr>
                                <td>
                                    Email:  <%# Eval("memEmail") %>
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    電話:  <%# Eval("memTel") %>
                                </td>
                            </tr>
                              <tr>
                                <td>
                                    地址:  <%# Eval("memCity") %> <%# Eval("memAdd") %>
                                </td>
                            </tr>
                                <tr>
                                <td>
                                    積分:  <%# Eval("memGrade") %>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>

                <%--<iframe src="member_info.aspx?memNo=1" class="modal-body" runat="server" id="infoframe" style="width:100%;height:680px;"></iframe>--%>
                <div class="modal-footer">
                    <h3>

                    </h3>
                </div>

            </div>
        </div>
    </div>
        </div>
        </div>
    </form>
</body>
</html>
