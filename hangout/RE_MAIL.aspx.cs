﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{
    public partial class RE_MAIL : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) { }
            else
            {
                string ID = Request.QueryString["contactNo"];
                SqlCommand sql = new SqlCommand("SELECT * FROM contact INNER JOIN member ON contact.memNo = member.memNo where contactNo=@id", cdnn);
                sql.Parameters.AddWithValue("@id", ID);
                SqlDataReader rd;
                cdnn.Open();
                rd = sql.ExecuteReader();
                if (rd.HasRows)
                {
                    while (rd.Read())
                    {
                        newsNo.Text = rd["contactNo"].ToString();
                        txttitle.Text = rd["contactTitle"].ToString();
                        txtcontent.Text = rd["contactText"].ToString();
                        txtdate.Text = rd["contactdate"].ToString();
                        txtre.Text = rd["contactAnswer"].ToString();
                    }
                }
                cdnn.Close();
            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand update = new SqlCommand("update contact set contactTitle=@title,contactText=@content,contactDate=@date,contactAnswer=@re,admNo=@adm where contactNo=@id", cdnn);
                update.Parameters.AddWithValue("@id", newsNo.Text);
                update.Parameters.AddWithValue("@title", txttitle.Text);
                update.Parameters.AddWithValue("@content", txtcontent.Text);
                update.Parameters.AddWithValue("@date", Convert.ToDateTime(txtdate.Text));
                update.Parameters.AddWithValue("@re", txtre.Text);
                update.Parameters.AddWithValue("@adm", Session["Id"].ToString());
                cdnn.Open();
                update.ExecuteNonQuery();
                cdnn.Close();


                Response.Redirect("contact.aspx");

            }
            catch(Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void btncancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("contact.aspx");
        }
    }
}