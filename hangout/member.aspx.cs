﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls; 
namespace hangout
{
    public partial class member : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
       
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "jump")
            {
                Response.Redirect("mamber_info.aspx?memNo=" + e.CommandArgument.ToString());
            }

            if (e.CommandName == "info")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                SqlDataSource2.SelectParameters["id"].DefaultValue = i.ToString();

                //ScriptManager.RegisterStartupScript(GetType(), "pop", "$('#info').modal('show')", true);
                ScriptManager.RegisterStartupScript(Page, GetType(), "pop", "$('#info').modal('show')", true);
               //infoframe.Src = "member_info.aspx?memNo=" + e.CommandArgument.ToString();    
            }
        }

        protected string GetGender(string gender)
        {
            if ((bool)Eval(gender))
                return "男";

            return "女";
        }

    } 
}