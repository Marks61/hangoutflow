﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace hangout
{
    public partial class activity : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["Hangoutnews"].ConnectionString);
        SqlConnection cenn = new SqlConnection(ConfigurationManager.ConnectionStrings["Hangoutnews"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {

            SqlCommand sql = new SqlCommand("SELECT * FROM  activity INNER JOIN member ON activity.memAcc = member.memAcc", cdnn);
            //sql.Parameters.AddWithValue("@id", Session["Id"].ToString());
            SqlDataAdapter d = new SqlDataAdapter(sql);
            DataSet dt = new DataSet();
            d.Fill(dt, "Contact");
            GridView1.DataSource = dt;
            if (!IsPostBack)
            {
                GridView1.DataBind();
            }



        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "info")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                SqlDataSource2.SelectParameters["id"].DefaultValue = i.ToString();

                //ScriptManager.RegisterStartupScript(GetType(), "pop", "$('#info').modal('show')", true);
                ScriptManager.RegisterStartupScript(Page, GetType(), "pop", "$('#info').modal('show')", true);
                //infoframe.Src = "member_info.aspx?memNo=" + e.CommandArgument.ToString();    
            }
            if (e.CommandName == "message")
            {
                int j = Convert.ToInt32(e.CommandArgument);
                SqlDataSource3.SelectParameters["id"].DefaultValue = j.ToString();

                //ScriptManager.RegisterStartupScript(GetType(), "pop", "$('#info').modal('show')", true);
                ScriptManager.RegisterStartupScript(Page, GetType(), "pop", "$('#messager').modal('show')", true);
                //ScriptManager.RegisterStartupScript(Page, GetType(), "pop", "$('#info').modal('show')", true);
                //infoframe.Src = "member_info.aspx?memNo=" + e.CommandArgument.ToString();   

                
            }
            if (e.CommandName == "canceling")
            { SqlCommand send = new SqlCommand("SELECT * FROM  activity INNER JOIN member ON activity.memAcc = member.memAcc where actNo=@id", cdnn);
                    send.Parameters.AddWithValue("@id", e.CommandArgument.ToString());
                    SqlDataReader rd;
                    cdnn.Open();
                    rd = send.ExecuteReader();
                    if (rd.HasRows)
                    {
                        while (rd.Read())
                        {Context.Session["check"]=rd["actOk"];
                            Context.Session["ame"] = rd["memName"].ToString();
                            Context.Session["activity"] = rd["actName"].ToString();
                            Context.Session["Mail"] = rd["memEmail"].ToString();
                            Context.Session["rec"] = rd["memNo"];
                        }
                    }
                    cdnn.Close();
                if (Session["check"].ToString() == "False")
                {
                    Response.Write("<script>alert('活動已經取消!!')</script>");

                }
                else
                {
                    SqlCommand canceo = new SqlCommand("update activity set actOk=@bool where actNo=@id", cdnn);
                    canceo.Parameters.AddWithValue("@id", e.CommandArgument.ToString());
                    canceo.Parameters.AddWithValue("@bool", false);
                    cdnn.Open();
                    canceo.ExecuteNonQuery();
                    cdnn.Close();


                   
                    SmtpClient mymail = new SmtpClient("smtp.gmail.com", 587);
                    mymail.Credentials = new NetworkCredential("hangout10801", "mcd10801");
                    mymail.EnableSsl = true;
                    string from = "hangout10801@gmail.com";
                    string to = Session["Mail"].ToString();
                    string subject = "活動停辦通知";
                    var body = "親愛的會員 [" + Session["ame"].ToString() + "]，您所主辦的活動[" + Session["activity"].ToString() + "]因為經過檢舉或是被管理員判斷違反活動規範，已被關閉。如認為本處分有所疑慮，請洽詢網站管理員。";
                    mymail.Send(from, to, subject, body);


                    string title = "活動停辦通知";
                    string time = DateTime.Now.ToString();
                    string content = "親愛的會員 [" + Session["ame"].ToString() + "]，您所主辦的活動[" + Session["activity"].ToString() + "]因為經過檢舉或是被管理員判斷違反活動規範，已被關閉。如認為本處分有所疑慮，請洽詢網站管理員。";
                    SqlCommand cmd = new SqlCommand("insert into mesAdm(memRec,mesAdmSub,mesAdmCon,mesAdmDate,admNo) values(@rec,@title,@content,@date,@account)", cdnn);

                    cmd.Parameters.AddWithValue("@rec", Session["rec"]);
                    cmd.Parameters.AddWithValue("@title", title.ToString());
                    cmd.Parameters.AddWithValue("@content", content.ToString());
                    cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(time));
                    cmd.Parameters.AddWithValue("@account", Session["Id"]);
                    cdnn.Open();
                    cmd.ExecuteNonQuery();
                    cdnn.Close();


                    SqlCommand mailfal = new SqlCommand("select * from activitylist inner join member on activitylist.memAcc=member.memAcc where activitylist.actNo=@no", cdnn);
                    mailfal.Parameters.AddWithValue("@no", e.CommandArgument.ToString());
                    SqlDataReader r;
                    cdnn.Open();
                    r = mailfal.ExecuteReader();
                    if (r.HasRows)
                    {
                        while (r.Read())
                        {
                            SmtpClient mmail = new SmtpClient("smtp.gmail.com", 587);
                            mmail.Credentials = new NetworkCredential("hangout10801", "mcd10801");
                            mmail.EnableSsl = true;
                            string fro = "hangout10801@gmail.com";
                            string t = r["memEmail"].ToString();
                            string ubject = "活動停辦通知";
                            var bdy = "親愛的會員 [" + r["memName"].ToString() + "]，您所報名參加的活動[" + Session["activity"].ToString() + "]因為經過檢舉或是被管理員判斷違反活動規範，已被關閉。如認為本處分有所疑慮，請洽詢網站管理員。";
                            mmail.Send(fro, t, ubject, bdy);

                            //Context.Session["memrec"] = r["memNo"].ToString();


                            string ti = "活動取消通知";
                            string me = DateTime.Now.ToString();
                            string mes = "親愛的會員 [" + r["memName"].ToString() + "]，您所參加的活動[" + Session["activity"].ToString() + "]因為經過檢舉或是被管理員判斷違反活動規範，已被關閉。如認為本處分有所疑慮，請洽詢網站管理員。";

                            SqlCommand mess = new SqlCommand("insert into mesAdm(memRec,mesAdmSub,mesAdmCon,mesAdmDate,admNo) values(@rec,@title,@content,@date,@account)", cenn);

                            mess.Parameters.AddWithValue("@rec", r["memNo"].ToString());
                            mess.Parameters.AddWithValue("@title", ti.ToString());
                            mess.Parameters.AddWithValue("@content", mes.ToString());
                            mess.Parameters.AddWithValue("@date", Convert.ToDateTime(me));
                            mess.Parameters.AddWithValue("@account", Session["Id"]);

                            cenn.Open();
                            mess.ExecuteNonQuery();
                            cenn.Close();

                        }

                    }
                    cdnn.Close();
                    Response.Redirect("activity.aspx");
                }




            }
            if (e.CommandName == "opening")
            {SqlCommand send = new SqlCommand("SELECT * FROM  activity INNER JOIN member ON activity.memAcc = member.memAcc where actNo=@id", cdnn);
                    send.Parameters.AddWithValue("@id", e.CommandArgument.ToString());
                    SqlDataReader rd;
                    cdnn.Open();
                    rd = send.ExecuteReader();
                    if (rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Context.Session["ame"] = rd["memName"].ToString();
                            Context.Session["activity"] = rd["actName"].ToString();
                            Context.Session["Mail"] = rd["memEmail"].ToString();
                            Context.Session["rec"] = rd["memNo"];
                        Context.Session["check"] = rd["actOk"];
                    }
                    }
                    cdnn.Close();
                if (Session["check"].ToString() == "True")
                {
                    Response.Write("<script>alert('活動已經開啟!!')</script>");

                }
                else
                {
                    SqlCommand ope = new SqlCommand("update activity set actOk=@bool where actNo=@id", cdnn);
                    ope.Parameters.AddWithValue("@id", e.CommandArgument.ToString());
                    ope.Parameters.AddWithValue("@bool", true);
                    cdnn.Open();
                    ope.ExecuteNonQuery();
                    cdnn.Close();

                    
                    SmtpClient mymail = new SmtpClient("smtp.gmail.com", 587);
                    mymail.Credentials = new NetworkCredential("hangout10801", "mcd10801");
                    mymail.EnableSsl = true;
                    string from = "hangout10801@gmail.com";
                    string to = Session["Mail"].ToString();
                    string subject = "活動復辦通知";
                    var body = "親愛的會員 [" + Session["ame"].ToString() + "]，您所主辦的活動[" + Session["activity"].ToString() + "]經過申復及再審查，認定該活動符合活動規範，已經重開。如認為本處分有所疑慮，請洽詢網站管理員。";
                    mymail.Send(from, to, subject, body);


                    string title = "活動復辦通知";
                    string time = DateTime.Now.ToString();
                    string content = "親愛的會員 [" + Session["ame"].ToString() + "]，您所主辦的活動[" + Session["activity"].ToString() + "]經過申復及再審查，認定該活動符合活動規範，已經重開。如認為本處分有所疑慮，請洽詢網站管理員。";
                    SqlCommand cmd = new SqlCommand("insert into mesAdm(memRec,mesAdmSub,mesAdmCon,mesAdmDate,admNo) values(@rec,@title,@content,@date,@account)", cdnn);

                    cmd.Parameters.AddWithValue("rec", Session["rec"]);
                    cmd.Parameters.AddWithValue("@title", title.ToString());
                    cmd.Parameters.AddWithValue("@content", content.ToString());
                    cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(time));
                    cmd.Parameters.AddWithValue("@account", Session["Id"]);
                    cdnn.Open();
                    cmd.ExecuteNonQuery();
                    cdnn.Close();



                    SqlCommand mailfal = new SqlCommand("select * from activitylist inner join member on activitylist.memAcc=member.memAcc where activitylist.actNo=@no", cdnn);
                    mailfal.Parameters.AddWithValue("@no", e.CommandArgument.ToString());
                    SqlDataReader r;
                    cdnn.Open();
                    r = mailfal.ExecuteReader();
                    if (r.HasRows)
                    {
                        while (r.Read())
                        {
                            SmtpClient mmail = new SmtpClient("smtp.gmail.com", 587);
                            mmail.Credentials = new NetworkCredential("hangout10801", "mcd10801");
                            mmail.EnableSsl = true;
                            string frm = "hangout10801@gmail.com";
                            string o = r["memEmail"].ToString();
                            string sject = "活動重開通知";
                            var by = "親愛的會員 [" + r["memName"].ToString() + "]，您所參加的活動[" + Session["activity"].ToString() + "]經過申復及再審查，認定該活動符合活動規範，已經重開。如認為本處分有所疑慮，請洽詢網站管理員。";
                            mmail.Send(frm, o, sject, by);

                            Context.Session["memrec"] = r["memNo"].ToString();

                            string tle = "活動重開通知";
                            string tie = DateTime.Now.ToString();
                            string conent = "親愛的會員 [" + r["memName"].ToString() + "]，您所參加的活動[" + Session["activity"].ToString() + "]經過申復及再審查，認定該活動符合活動規範，已經重開。如認為本處分有所疑慮，請洽詢網站管理員。";
                            SqlCommand plusmes = new SqlCommand("insert into mesAdm(memRec,mesAdmSub,mesAdmCon,mesAdmDate,admNo) values(@rec,@title,@content,@date,@account)", cenn);

                            plusmes.Parameters.AddWithValue("@rec", Session["memrec"]);
                            plusmes.Parameters.AddWithValue("@title", tle.ToString());
                            plusmes.Parameters.AddWithValue("@content", conent.ToString());
                            plusmes.Parameters.AddWithValue("@date", Convert.ToDateTime(tie));
                            plusmes.Parameters.AddWithValue("@account", Session["Id"]);
                            cenn.Open();
                            plusmes.ExecuteNonQuery();
                            cenn.Close();

                        }
                    }
                    cdnn.Close();
                    Response.Redirect("activity.aspx");
                }
            }
        }
    
        protected string Getfree(string free)
        {
            if ((bool)Eval(free))
                return "需要費用";

            return "無料參加";
        }
        protected string GetPublic(string publict)
        {
            if ((bool)Eval(publict))
                return "公開";

            return "非公開";
        }
        protected string Getopen(string open)
        {
            if ((bool)Eval(open))
                return "活動成立";

            return "活動取消";
        }
    }

}