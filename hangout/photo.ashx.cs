﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace hangout
{
    /// <summary>
    /// photo 的摘要描述
    /// </summary>
    public class photo : IHttpHandler
    {
        SqlConnection cdnn=new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);
        public void ProcessRequest(HttpContext context)
        {
          
            SqlCommand sql = new SqlCommand("SELECT vioImg FROM  violation where vioNo =@id", cdnn);
            sql.Parameters.AddWithValue("@id",context.Request.QueryString["vioNo"]);
            SqlDataReader rd;
            cdnn.Open();
            rd = sql.ExecuteReader();
            if (rd.HasRows)
            {
                if (rd.Read())
                {
                    context.Response.BinaryWrite((byte[])rd["vioImg"]);
                }
            }
            cdnn.Close();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}