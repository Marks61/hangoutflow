﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="contact.aspx.cs" Inherits="hangout.contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>未處理信件</h2>
     <asp:gridview id="gridview2" Cssclass="table" autoGenerateColumns="False" GridLines="None" OnRowCommand="gridview1_RowCommand"  PageSize="15" runat="server" DataKeyNames="contactNo">
        <Columns>
            <asp:BoundField DataField="contactNo" HeaderText="信件編號" InsertVisible="False" ReadOnly="True" SortExpression="contactNo" />
            <asp:BoundField DataField="memName" HeaderText="寄件者(會員)" SortExpression="memNo" />
            <asp:BoundField DataField="contactTitle" HeaderText="主旨" SortExpression="contactTitle" />
            <asp:BoundField DataField="contactText" HeaderText="內容" SortExpression="contactText" />
            <asp:BoundField DataField="contactDate" HeaderText="寄件日期" SortExpression="contactDate" />
            <asp:BoundField DataField="contactAnswer" HeaderText="回覆" SortExpression="contactAnswer" />
             <asp:TemplateField>
               <ItemTemplate>
                    <asp:LinkButton CommandName="info" CommandArgument='<%# Eval("contactNo") %>' ID="LinkButton2" type="button" runat="server" class="btn btn-info">回覆</asp:LinkButton>
               </ItemTemplate> 
                      
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <h2>私人信件</h2>
<%--    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hangOutnews %>" SelectCommand="SELECT * FROM  (contact  INNER JOIN member ON contact.memNo = member.memNo)INNER JOIN administrator
ON contact.admNo = administrator.admNo where"></asp:SqlDataSource>--%>
     <asp:gridview id="gridview1" Cssclass="table" autoGenerateColumns="False" GridLines="None" OnRowCommand="gridview1_RowCommand"  PageSize="15" runat="server" DataKeyNames="contactNo">
        <Columns>
            <asp:BoundField DataField="contactNo" HeaderText="信件編號" InsertVisible="False" ReadOnly="True" SortExpression="contactNo" />
            <asp:BoundField DataField="memName" HeaderText="寄件者(會員)" SortExpression="memNo" />
            <asp:BoundField DataField="contactTitle" HeaderText="主旨" SortExpression="contactTitle" />
            <asp:BoundField DataField="contactText" HeaderText="內容" SortExpression="contactText" />
            <asp:BoundField DataField="contactDate" HeaderText="寄件日期" SortExpression="contactDate" />
            <asp:BoundField DataField="contactAnswer" HeaderText="回覆" SortExpression="contactAnswer" />
            <%-- <asp:TemplateField>
               <ItemTemplate>
                    <asp:LinkButton CommandName="info" CommandArgument='<%# Eval("contactNo") %>' ID="LinkButton2" type="button" runat="server" class="btn btn-info">回覆</asp:LinkButton>
               </ItemTemplate> 
                      
            </asp:TemplateField>--%>
        </Columns>
    </asp:GridView>
</asp:Content>
