﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using hangout.App_Code;

namespace hangout
{
    public partial class sign_up : System.Web.UI.Page
    {
        SqlConnection cdnn = new SqlConnection(ConfigurationManager.ConnectionStrings["HangOutlogin"].ConnectionString);

     
        protected void Page_Load(object sender, EventArgs e)
        {
          
            SqlCommand chk = new SqlCommand("select * from administrator where admAcc=@account", cdnn);
            chk.Parameters.AddWithValue("@account", Session["Account"].ToString());
            SqlDataReader rd;
            cdnn.Open();
            rd = chk.ExecuteReader();
           
            if (rd.HasRows)
            { 
                while (rd.Read()) {
                    bool chkk =Convert.ToBoolean(rd["admHighest"]);
                    if(chkk ==false)
                    Response.Redirect("error.aspx");
                    else { }
                }
            }
            cdnn.Close();
        }
        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            SqlCommand trast = new SqlCommand("select * from administrator where admAcc=@account", cdnn);
            trast.Parameters.AddWithValue("@account", txtAcc.Text);
            SqlDataReader rd;
            cdnn.Open();
            rd = trast.ExecuteReader();
            if (rd.HasRows)
                args.IsValid = false;
            else
                args.IsValid = true;
            cdnn.Close();

        }

      
        protected void btnSignup_Click(object sender, EventArgs e)
        {
        
                try
                {
                pass psw = new pass();
                    SqlCommand cmd = new SqlCommand("insert into administrator values(@account,@psw,@Name,@chk,@mail)", cdnn);
                                       cmd.Parameters.AddWithValue("@account", txtAcc.Text);
                    cmd.Parameters.AddWithValue("@name", txtName.Text);
                    cmd.Parameters.AddWithValue("@psw", psw.Change(txtPsw.Text));
                cmd.Parameters.AddWithValue("@mail", txtMail.Text);
                    if (CheckBox1.Checked)
                    {
                        cmd.Parameters.AddWithValue("@chk", true);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@chk", false);
                    }
                

                    cdnn.Open();
                    cmd.ExecuteNonQuery();
                    cdnn.Close();

                Label1.Text = txtAcc.Text + "註冊成功。 由管理員" + Session["Account"].ToString() + "完成註冊。";
                }
                catch
                {
                Label1.Text = "出現錯誤。請聯絡資料庫管理員。";
                }
            
        }

        protected void btncancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        
    }
}